module.exports = {
    "verbose": true,
    "roots": [ "./src" ],
    "testMatch": [
        "**/*.test.js"
    ],
    "moduleFileExtensions": [ "styl", "js" ],
    "moduleDirectories": [ "..", "../src", "../node_modules" ],
    "moduleNameMapper": {
        ".*\\.(css|styl)$": "identity-obj-proxy",
        ".*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/mocks/file.js" // eslint-disable-line
    },
    "transform": {
        "^.+\\.js$": "babel-jest"
    },
    "collectCoverage": true,
    "collectCoverageFrom": [
        "src/**/*.js"
    ]
};