const path = require("path");
const webpack = require("webpack");
const config = require("common-config");
const VirtualModulePlugin = require("virtual-module-webpack-plugin");
const filepaths = require("../filepaths");

module.exports = {
    entry: {
        main: filepaths.examples.src.index_js,
        vendor: [
            "babel-polyfill",
            "config",
            "react",
            "react-dom",
            "react-redux",
            "react-router",
            "darch/src"
        ]
    },
    output: {
        publicPath: "/assets/",
        chunkFilename: "[chunkhash].js",
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: "file-loader", options: {
                            name: "/fonts/[name].[ext]"
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.styl$/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader", options: {
                            modules: true,
                            sourceMap: true,
                            camelCase: true,
                            localIdentName: "[hash:base64:5]"
                        }
                    },
                    {
                        loader: "stylus-loader", options: {
                            preferPathResolver: "webpack"
                        }
                    }
                ]
            },

            {
                test: /.jsx?$/,
                enforce: "pre",
                exclude: /node_modules|lodash|config/,
                use: [
                    {
                        loader: "eslint-loader", options: {
                            emitWarning: true
                        }
                    }
                ]
            },

            {
                test: /.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            babelrc: "../.babelrc"
                        }
                    }
                ]
            },

            {
                test: /\.(png|jpg)$/,
                use: [
                    {
                        loader: "url-loader", options: {
                            limit: 8192,
                            name: "images/[sha512:hash:base64:7].[ext]",
                            publicPath: "/assets/"
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        alias: {
            "darch": path.resolve(__dirname, "..")
        },
        modules: [
            path.resolve(__dirname, ".."),
            path.resolve(__dirname, "../src"),
            path.resolve(__dirname, "../examples/src"),
            path.resolve(__dirname, "../node_modules")
        ],
        extensions: [ ".js", ".jsx", ".styl", ".css", ".png", ".jpg" ]
    },
    node: {
        console: true,
        process: true,
        __filename: "mock",
        __dirname: "mock",
        fs: "empty",
        net: "empty",
        tls: "empty"
    },
    plugins: [
        new webpack.EnvironmentPlugin([
            "NODE_ENV"
        ]),
        new VirtualModulePlugin({
            moduleName: "config.js",
            contents: `module.exports = ${JSON.stringify(config)}`
        }),
        new webpack.optimize.CommonsChunkPlugin({
            names: [ "vendor" ],
            filename: "common.js",
            minChunks: Infinity
        })
    ]
};
