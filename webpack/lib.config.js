const path = require("path");
const webpack = require("webpack");
const VirtualModulePlugin = require("virtual-module-webpack-plugin");

module.exports = {
    entry: {
        "main": "./src/main/index.js",
        "utils/index": "./src/utils/index.js",
        "utils/logger": "./src/utils/logger.js",
        "utils/redux": "./src/utils/redux.js"
    },
    externals: [
        "ace-builds",
        "axios",
        "babel-polyfill",
        "classnames",
        "dom-helpers",
        "highlightjs",
        "hogan.js",
        "lodash",
        "moment",
        "crypto",
        /^prismjs/,
        "qs",
        "react",
        "react-addons-css-transition-group",
        "react-dom",
        "react-input-autosize",
        "react-redux",
        "react-router",
        "react-router-redux",
        "redux",
        "redux-actions",
        "redux-promise-middleware",
        function (context, request, callback) {
            const rgx = /^darch\/src\/(.*)/;

            if (rgx.test(request)) {
                const newRequest = request.replace(rgx, "darch/lib/$1");
                return callback(null, newRequest);
            }

            callback();
        }
    ],
    output: {
        path: "./lib",
        filename: "[name].js",
        library: "darch",
        libraryTarget: "umd"
    },
    module: {
        rules: [
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                use: [
                    { loader: "ignore-loader" }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.styl$/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                            sourceMap: false,
                            camelCase: true,
                            localIdentName: "[hash:base64:5]"
                        }
                    },
                    {
                        loader: "stylus-loader",
                        options: {
                            preferPathResolver: "webpack"
                        }
                    }
                ]
            },

            {
                test: /.jsx?$/,
                enforce: "pre",
                exclude: /node_modules|lodash|config/,
                use: [
                    {
                        loader: "eslint-loader",
                        options: {
                            emitWarning: true
                        }
                    }
                ]
            },

            {
                test: /.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            babelrc: "../.babelrc"
                        }
                    }
                ]
            },

            {
                test: /\.(png|jpg)$/,
                use: [
                    {
                        loader: "url-loader", options: {
                            limit: 8192,
                            name: "images/[sha512:hash:base64:7].[ext]",
                            publicPath: "/assets/"
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        alias: {
            "darch": path.resolve(__dirname, "..")
        },
        modules: [
            path.resolve(__dirname, "../"),
            path.resolve(__dirname, "../src"),
            path.resolve(__dirname, "../node_modules")
        ],
        extensions: [".js", ".jsx", ".styl", ".css", ".png", ".jpg"]
    },
    node: {
        console: true,
        process: true,
        __filename: "mock",
        __dirname: "mock",
        fs: "empty",
        net: "empty",
        tls: "empty"
    },
    plugins: [
        new webpack.EnvironmentPlugin([
            "NODE_ENV"
        ]),
        new VirtualModulePlugin({
            moduleName: "config.js",
            contents: `module.exports = ${JSON.stringify(config)}`
        }),
    ]
};
