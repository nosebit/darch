const lodash = require("lodash");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const baseConfig = require("./examples.config");

// Merge with base config
const config = lodash.merge({}, baseConfig, {
    plugins: baseConfig.plugins.concat([
        new UglifyJsPlugin()
    ])
});

// Export config
module.exports = config;