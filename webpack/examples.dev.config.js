const lodash = require("lodash");
const filepaths = require("../filepaths");
const baseConfig = require("./examples.config");

// Merge with base config
const config = lodash.merge({}, baseConfig, {
    output: {
        path: filepaths.examples.devDest + "/assets/"
    },
    watch: true,
    devtool: "source-map"
});

// Export config
module.exports = config;