import "babel-polyfill";
import React from "react";
import { createBrowserHistory } from "history";
import {
    ConnectedRouter,
    routerReducer,
    routerMiddleware
} from "react-router-redux";
import { Provider } from "react-redux";
import { render } from "react-dom";

import config from "config";
import Pager from "darch/src/pager";
import i18n from "darch/src/i18n";
import Redux from "darch/src/utils/redux";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("index");

/**
 * App Bootstrap
 *
 * @returns {void}
 */
function bootstrap() {
    const logger = Logger.create("bootstrap");
    logger.info("enter", { config });

    // Create a browser history
    const history = createBrowserHistory();

    // Build the middleware for intercepting and dispatching navigation actions
    const middleware = routerMiddleware(history);

    // Instantiate redux
    // Create redux store with app reducers
    new Redux({
        routing: routerReducer,
        i18n: i18n.reducer
    }, {
        shared: true,
        middlewares: [
            middleware
        ]
    });

    // Specify the main route
    const routes = (
        <Provider store={Redux.shared.store}>
            <ConnectedRouter history={history}>
                {Pager.utils.renderRoutes({
                    routes: [ require("./app/route") ]
                })}
            </ConnectedRouter>
        </Provider>
    );    

    // Render routes
    render(routes, document.getElementById("main"));
}


/****************************************************************
* Run Bootstrap
****************************************************************/
bootstrap();