import React from "react";
import { Link } from "react-router-dom";
import LoggerFactory from "darch/src/utils/logger";
import i18n from "darch/src/i18n";
import Container from "darch/src/container";

const Logger = new LoggerFactory("app.i18n");

/**
 * Main component
 */
class Home extends React.Component {
    static propTypes = {};

    state = {};

    /**
     * React lifecycle hook
     *
     * @returns {void}
     */
    async componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * Renders the component
     *
     * @returns {element} - The component elements
     */
    render() {
        return (
            <div>
                <Container>
                    <h1>
                        <i18n.Text value="_TITLE_" />
                    </h1>

                    <p>
                        <i18n.Text value="_INTRO_" />
                    </p>

                    <ul>
                        <li>
                            <Link to={i18n.utils.routePath("app.i18n")}>
                                <i18n.Text value="_I18N_TITLE_" />
                            </Link>
                        </li>
                    </ul>
                </Container>
            </div>
        );
    }
}

export default Home;