import React from "react";
import dedent from "dedent";
import styles from "examples/src/app/styles";
import LoggerFactory from "darch/src/utils/logger";
import Section from "darch/src/section";
import i18n from "darch/src/i18n";
import Code from "darch/src/code";
import Label from "darch/src/label";
import Container from "darch/src/container";

const Logger = new LoggerFactory("app.i18n");

/**
 * App component
 *
 * @class Component
 * @extends {React.Component}
 */
class I18n extends React.Component {
    static propTypes = {};

    state = {};

    /**
     * React lifecycle hook
     *
     * @returns {void}
     */
    async componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * Renders the component
     *
     * @returns {element} - The component elements
     */
    render() {
        return (
            <div>
                <Container>
                    <h1>
                        <i18n.Text
                            value="_I18N_TITLE_"
                            className={styles.title}
                        />
                        <Label
                            scale={0.8}
                            color="moody"
                        >
                            <i18n.Text value="_SECTION_TYPE_MODULE_" />
                        </Label>
                    </h1>

                    <Section>
                        <p>
                            <i18n.Text value="_I18N_INTRO_TRANSLATION_FILE_" />
                        </p>

                        {/*eslint-disable*/}
                        <Code
                            lang="json"
                            value={dedent`{
                                "lang": "en-us",

                                "formats": {
                                    "datetime": "YYYY-MM-DD HH:mm:ss",
                                    "date": "YYYY-MM-DD",
                                    "time": "HH:mm:ss",
                                    "currency": "US$"
                                },

                                "routePaths": {
                                    "app": "/",
                                    "app.i18n": "/i18n",
                                    "app.i18n.example": "/i18n/example"
                                },

                                "dictionary": {
                                    "_SOME_TEXT_KEY_": "The translation for this key",
                                    "_SOME_TEXT_WITH_HTML_KEY_": "Hi, i have <b>html</b> tags.",
                                    "_SOME_TEXT_WITH_VARIABLES_": "Hi {username}, i can have <u>variables</u> too!"
                                }
                            }`}
                        />
                        {/*eslint-enable*/}
                    </Section>

                    <Section>
                        <h4>
                            <i18n.Text
                                value="_I18N_EXAMPLE_1_TITLE_"
                            />
                        </h4>

                        {/*eslint-disable*/}
                        <Code
                            lang="json"
                            value={dedent`{
                                "lang": "en-us",

                                "formats": {
                                    "datetime": "YYYY-MM-DD HH:mm:ss",
                                    "date": "YYYY-MM-DD",
                                    "time": "HH:mm:ss",
                                    "currency": "US$"
                                },

                                "routePaths": {
                                    "app": "/",
                                    "app.i18n": "/i18n",
                                    "app.i18n.example": "/i18n/example"
                                },

                                "dictionary": {
                                    "_SOME_TEXT_KEY_": "The translation for this key",
                                    "_SOME_TEXT_WITH_HTML_KEY_": "Hi, i have <b>html</b> tags.",
                                    "_SOME_TEXT_WITH_VARIABLES_": "Hi {username}, i can have <u>variables</u> too!"
                                }
                            }`}
                        />
                        {/*eslint-enable*/}
                    </Section>
                </Container>
            </div>
        );
    }
}

export default I18n;