import React from "react";
import Pager from "darch/src/pager";
import loadPage from "bundle-loader?lazy!./index";

module.exports = {
    id: "app.i18n",

    Page: (props) => (
        <Pager load={loadPage}>
            {(Page) => (
                <Page {...props} />
            )}
        </Pager>
    ),

    routes: [],

    loadPage
};
