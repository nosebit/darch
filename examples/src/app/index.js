import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Component from "./component";
import i18n from "darch/src/i18n";

/**
 * Redux map state to props function.
 *
 * @param {object} state - The redux state
 * @param {object} ownProps - A list of props passed to the component.
 * @returns {object} Props the should be injected into the component.
 */
function mapStateToProps(state) {
    return {
        spec: state.i18n.spec
    };
}

/**
 * Redux dispatch to props map.
 * 
 * @param {function} dispatch - Redux dispatch function.
 * @returns {object} Props the should be injected into the component.
 */
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({
            i18NInit: i18n.actions.i18NInit
        }, dispatch)
    };
}

// Compose with redux
const Composed = connect(
    mapStateToProps,
    mapDispatchToProps
)(Component);

// export
export default Composed;
