import React from "react";
import dedent from "dedent";
import styles from "../../styles";
import LoggerFactory from "darch/src/utils/logger";
import Section from "darch/src/section";
import i18n from "darch/src/i18n";
import Code from "darch/src/code";
import Label from "darch/src/label";
import Headline from "darch/src/headline";

const Logger = new LoggerFactory("app.sections.label");

/**
 * App component
 *
 * @class Component
 * @extends {React.Component}
 */
class LabelSection extends React.Component {
    static propTypes = {};

    state = {};

    /**
     * React lifecycle hook
     *
     * @returns {void}
     */
    async componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * Renders the component
     *
     * @returns {element} - The component elements
     */
    render() {
        return (
            <Section>
                <h2>
                    <Headline>
                        <i18n.Text
                            value="_LABEL_SECTION_TITLE_"
                            className={styles.title}
                        />
                        <Label
                            scale={0.8}
                            color="warning"
                        >
                            <i18n.Text value="_SECTION_TYPE_COMPONENT_" />
                        </Label>
                    </Headline>
                </h2>

                <p>
                    <i18n.Text
                        value="_LABEL_SECTION_DESCRIPTION_"
                    />
                </p>

                <Section>
                    <h6>
                        <i18n.Text 
                            value="_SECTION_EXAMPLE_TITLE_"
                        />
                    </h6>

                    <Code
                        lang="jsx"
                        value={dedent`
                            <Label
                                layout="flat"
                                color="#26A65B"
                            >
                                <i18n.Text value="_SOME_TEXT_" />
                            </Label>
                        `}
                    />
                </Section>

                <Section>
                    <h6>
                        <i18n.Text
                            value="_SECTION_EXAMPLE_RESULT_TITLE_"
                        />
                    </h6>

                    <div className={styles.resultBox}>
                        <Label
                            layout="flat"
                            color="#26A65B"
                        >
                            <i18n.Text value="_SOME_TEXT_"/>
                        </Label>
                    </div>
                </Section>
            </Section>
        );
    }
}

export default LabelSection;