import React from "react";
import dedent from "dedent";
import styles from "../../styles";
import LoggerFactory from "darch/src/utils/logger";
import Section from "darch/src/section";
import i18n from "darch/src/i18n";
import Code from "darch/src/code";
import Label from "darch/src/label";
import Headline from "darch/src/headline";

const Logger = new LoggerFactory("app.sections.usage");

/**
 * App component
 *
 * @class Component
 * @extends {React.Component}
 */
class ContainerSection extends React.Component {
    static propTypes = {};

    state = {};

    /**
     * React lifecycle hook
     *
     * @returns {void}
     */
    async componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * Renders the component
     *
     * @returns {element} - The component elements
     */
    render() {
        return (
            <Section>
                <h2>
                    <Headline>
                        <i18n.Text
                            value="_CONTAINER_SECTION_TITLE_"
                            className={styles.title}
                        />
                        <Label
                            scale={0.8}
                            color="warning"
                        >
                            <i18n.Text value="_SECTION_TYPE_COMPONENT_" />
                        </Label>
                    </Headline>
                </h2>

                <p>
                    <i18n.Text
                        value="_CONTAINER_SECTION_DESCRIPTION_"
                    />
                </p>

                <Section>
                    <h6>
                        <i18n.Text 
                            value="_SECTION_EXAMPLE_TITLE_"
                        />
                    </h6>

                    <Code
                        lang="jsx"
                        value={dedent`
                            <Container>
                                This text is inside a container.
                            </Container>
                        `}
                    />
                </Section>

                <Section>
                    <h6>
                        <i18n.Text
                            value="_SECTION_COMPONENT_PROPS_TITLE_"
                        />
                    </h6>

                    <div className={styles.propTableContainer}>
                        <table className={styles.propTable}>
                            <thead>
                                <tr>
                                    <th>
                                        <i18n.Text
                                            value="_PROP_TABLE_NAME_TH_"
                                        />
                                    </th>
                                    <th>
                                        <i18n.Text
                                            value="_PROP_TABLE_TYPE_TH_"
                                        />
                                    </th>
                                    <th>
                                        <i18n.Text
                                            value="_PROP_TABLE_VALUES_TH_"
                                        />
                                    </th>
                                    <th>
                                        <i18n.Text
                                            value="_PROP_TABLE_DEFAULT_TH_"
                                        />
                                    </th>
                                    <th>
                                        <i18n.Text
                                            value="_PROP_TABLE_DESCRIPTION_TH_"
                                        />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <i18n.Text value="size" />
                                    </td>
                                    <td>
                                        <i18n.Text value="string" />
                                    </td>
                                    <td>
                                        <ul className={styles.propsList}>
                                            <li>
                                                <i18n.Text
                                                    value="sm"
                                                />
                                            </li>
                                            <li>
                                                <i18n.Text
                                                    value="md"
                                                />
                                            </li>
                                            <li>
                                                <i18n.Text
                                                    value="lg"
                                                />
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <i18n.Text value="lg" />
                                    </td>
                                    <td>
                                        <i18n.Text
                                            value="_CONTAINER_SECTION_PROP_SIZE_DESCRIPTION_" //eslint-disable-line
                                        />
                                    </td> 
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </Section>
            </Section>
        );
    }
}

export default ContainerSection;