import React from "react";
import dedent from "dedent";
import LoggerFactory from "darch/src/utils/logger";
import i18n from "darch/src/i18n";
import Section from "darch/src/section";
import Code from "darch/src/code";

const Logger = new LoggerFactory("app.sections.intro");

/**
 * App component
 *
 * @class Component
 * @extends {React.Component}
 */
class IntroSection extends React.Component {
    static propTypes = {};

    state = {};

    /**
     * React lifecycle hook
     *
     * @returns {void}
     */
    async componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * Renders the component
     *
     * @returns {element} - The component elements
     */
    render() {
        return (
            <Section>
                <p>
                    <i18n.Text
                        value="_INTRO_SECTION_MAIN_"
                    />
                </p>

                <h3>
                    <i18n.Text
                        value="_INTRO_SECTION_USAGE_SECTION_TITLE_"
                    />
                </h3>
                
                <p>
                    <i18n.Text
                        value="_INTRO_SECTION_USAGE_SECTION_INSTALL_"
                    />
                </p>

                <Code
                    lang="bash"
                    value="npm install --save darch"
                />

                <p>
                    <i18n.Text
                        value="_INTRO_SECTION_USAGE_SECTION_AFTER_INSTALL_"
                    />
                </p>

                <Code
                    lang="js"
                    value={dedent`
                        import Form from "darch/lib/form";

                        // For old school guys, you can do:
                        // const Form = require("darch/lib/form")
                    `}
                />

                <p>
                    <i18n.Text
                        value="_INTRO_SECTION_USAGE_SECTION_IMPORT_INDIVIDUAL_MODULES_" //eslint-disable-line
                    />
                </p>

                <Code
                    lang="js"
                    value={dedent`
                        import {Form, Field, Container} from "darch";

                        // For old school guys, you have to do:
                        // const {Form, Field, Container} = require("darch");

                        // And for ES5 prehistoric guys:
                        // const Darch = require("darch");
                        // const Form = Darch.Form;
                    `}
                />
            </Section>
        );
    }
}

export default (props) => (
    <IntroSection {...props} />
);