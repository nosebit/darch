import React from "react";
import dedent from "dedent";
import styles from "../../styles";
import LoggerFactory from "darch/src/utils/logger";
import Section from "darch/src/section";
import i18n from "darch/src/i18n";
import Code from "darch/src/code";
import Label from "darch/src/label";
import Headline from "darch/src/headline";

const Logger = new LoggerFactory("app.sections.usage");

/**
 * App component
 *
 * @class Component
 * @extends {React.Component}
 */
class ContainerSection extends React.Component {
    static propTypes = {};

    state = {};

    /**
     * React lifecycle hook
     *
     * @returns {void}
     */
    async componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * Renders the component
     *
     * @returns {element} - The component elements
     */
    render() {
        return (
            <Section>
                <h2>
                    <Headline>
                        <i18n.Text
                            value="_CONTAINER_SECTION_TITLE_"
                            className={styles.title}
                        />
                        <Label
                            scale={0.8}
                            color="moody"
                        >
                            <i18n.Text value="_SECTION_TYPE_MODULE_" />
                        </Label>
                    </Headline>
                </h2>

                <p>
                    <i18n.Text
                        value="_CONTAINER_SECTION_DESCRIPTION_"
                    />
                </p>

                <Section>
                    <h6>
                        <i18n.Text 
                            value="_SECTION_EXAMPLE_TITLE_"
                        />
                    </h6>

                    <Code
                        lang="jsx"
                        value={dedent`
                            <Container>
                                This text is inside a container.
                            </Container>
                        `}
                    />
                </Section>
            </Section>
        );
    }
}

export default (props) => (
    <ContainerSection {...props} />
);