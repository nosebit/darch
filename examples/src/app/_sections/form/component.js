import React from "react";
import styles from "../../styles";
import LoggerFactory from "darch/src/utils/logger";
import Section from "darch/src/section";
import i18n from "darch/src/i18n";
import Label from "darch/src/label";
import Headline from "darch/src/headline";
import Form from "darch/src/form";
import Field from "darch/src/field";
import Text from "darch/src/text";
import Button from "darch/src/button";

const Logger = new LoggerFactory("app.sections.usage");

/**
 * App component
 *
 * @class Component
 * @extends {React.Component}
 */
class FormSection extends React.Component {
    static propTypes = {};

    state = {};

    /**
     * React lifecycle hook
     *
     * @returns {void}
     */
    async componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * Renders the component
     *
     * @returns {element} - The component elements
     */
    render() {
        return (
            <Section>
                <h2>
                    <Headline>
                        <i18n.Text
                            value="_FORM_SECTION_TITLE_"
                            className={styles.title}
                        />
                        <Label
                            scale={0.8}
                            color="moody"
                        >
                            <i18n.Text value="_SECTION_TYPE_MODULE_" />
                        </Label>
                    </Headline>
                </h2>

                <Section>
                    <Form loading={this.state.formLoading}
                        onSubmit={() => {
                            this.setState({ formLoading: true });

                            setTimeout(() => {
                                this.setState({ formLoading: false });
                            }, 5000);
                        }}>
                        <Field.Section>
                            <Text scale={0.8}>
                                <i18n.Text value="email" />
                            </Text>
                            <Field.Text
                                name="test"
                                placeholder="my@email.com"
                                scale={1.5}
                                validators="$required|$email" />
                            <Field.Error
                                for="test"
                                validator="$required"
                                message="email is required" />
                            <Field.Error
                                for="test"
                                validator="$email"
                                message="enter a valid email" />
                        </Field.Section>

                        <Field.Section>
                            <div>
                                <Button
                                    type="submit"
                                    scale={1.5}
                                >
                                    <i18n.Text value="send" />
                                </Button>
                            </div>
                        </Field.Section>
                    </Form>
                </Section>
            </Section>
        );
    }
}

export default (props) => (
    <FormSection {...props} />
);