import React from "react";
import dedent from "dedent";
import styles from "../../styles";
import LoggerFactory from "darch/src/utils/logger";
import Section from "darch/src/section";
import i18n from "darch/src/i18n";
import Code from "darch/src/code";
import Label from "darch/src/label";
import Headline from "darch/src/headline";

const Logger = new LoggerFactory("app.sections.label");

/**
 * App component
 *
 * @class Component
 * @extends {React.Component}
 */
class I18nSection extends React.Component {
    static propTypes = {};

    state = {};

    /**
     * React lifecycle hook
     *
     * @returns {void}
     */
    async componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * Renders the component
     *
     * @returns {element} - The component elements
     */
    render() {
        return (
            <Section>
                <h2>
                    <Headline>
                        <i18n.Text
                            value="_I18N_SECTION_TITLE_"
                            className={styles.title}
                        />
                        <Label
                            scale={0.8}
                            color="moody"
                        >
                            <i18n.Text value="_SECTION_TYPE_MODULE_" />
                        </Label>
                    </Headline>
                </h2>

                <p>
                    <i18n.Text
                        value="_I18N_SECTION_DESCRIPTION_"
                    />
                </p>

                {/*eslint-disable*/}
                <Code
                    lang="json"
                    value={dedent`{
                        "lang": "en-us",

                        "formats": {
                            "datetime": "YYYY-MM-DD HH:mm:ss",
                            "date": "YYYY-MM-DD",
                            "time": "HH:mm:ss",
                            "currency": "US$"
                        },

                        "routePaths": {
                            "app": "/",
                            "app.home": "/home",
                            "app.basket": "/basket",
                            "app.basket.items": "/basket/items",
                            "app.basket.items.item": "/baskett/items/:item"
                        },

                        "dictionary": {
                            "_SOME_TEXT_KEY_": "The translation for this key",
                            "_SOME_TEXT_WITH_HTML_KEY_": "Hi, i have <b>html</b> tags.",
                            "_SOME_TEXT_WITH_VARIABLES_": "Hi {username}, i can have <u>variables</u> too!"
                        }
                    }`}
                />
                {/*eslint-enable*/}

                <p>
                    <i18n.Text
                        value="_I18N_SECTION_TRANSLATION_FILE_DESCRIPTION_"
                    />
                </p>

                <ul>
                    <li>
                        <i18n.Text
                            value="_I18N_SECTION_TRANSLATION_FILE_FORMATS_ITEM_" //eslint-disable-line
                        />
                    </li>

                    <li>
                        <i18n.Text
                            value="_I18N_SECTION_TRANSLATION_FILE_ROUTE_PATHS_ITEM_" //eslint-disable-line
                        />
                    </li>

                    <li>
                        <i18n.Text
                            value="_I18N_SECTION_TRANSLATION_FILE_DICTIONARY_ITEM_" //eslint-disable-line
                        />
                    </li>
                </ul>

                <Section>
                    <h6>
                        <i18n.Text
                            value="_I18N_SECTION_INITIALIZATION_SECTION_TITLE_"
                        />
                    </h6>

                    <p>
                        <i18n.Text
                            value="_I18N_SECTION_INITIALIZATION_SECTION_DESCRIPTION_" // eslint-disable-line
                        />
                    </p>

                    <Code
                        lang="js"
                        value={dedent`
                            import { bindActionCreators } from "redux";
                            import { connect } from "react-redux";
                            import i18n from "darch/lib/i18n";

                            class Root extend React.Component {
                                componentDidMount() {
                                    await Promise.all([
                                        this.props.actions.i18NInit("pt-br", {
                                            fallbackLang = "en-us",
                                            url = "/assets/i18n"
                                        })
                                    ])

                                    this.setState({initialized: true});
                                }

                                render() {
                                    if(!this.state.initialized) {
                                        returns null;
                                    }

                                    return (
                                        <div>
                                            {'My App'}
                                        </div>
                                    )
                                }
                            }

                            function mapStateToProps() {
                                return {};
                            }

                            function mapDispatchToProps(dispatch) {
                                return {
                                    actions: bindActionCreators({
                                        i18NInit: i18n.actions.i18NInit
                                    }, dispatch)
                                };
                            }

                            /** Export **/
                            export default connect(
                                mapStateToProps,
                                mapDispatchToProps
                            )(Root);
                        `}
                    />

                    <p>
                        <i18n.Text
                            value="_I18N_SECTION_INITIALIZATION_SECTION_EXAMPLE_EXPLANATION_" // eslint-disable-line
                        />
                    </p>
                </Section>

                <Section>
                    <h6>
                        <i18n.Text 
                            value="_SECTION_EXAMPLE_TITLE_"
                        />
                    </h6>

                    <Code
                        lang="jsx"
                        value={dedent`
                            <Label
                                layout="flat"
                                color="#26A65B"
                            >
                                <i18n.Text value="_SOME_TEXT_" />
                            </Label>
                        `}
                    />
                </Section>

                <Section>
                    <h6>
                        <i18n.Text
                            value="_SECTION_EXAMPLE_RESULT_TITLE_"
                        />
                    </h6>

                    <div className={styles.resultBox}>
                        <Label
                            layout="flat"
                            color="#26A65B"
                        >
                            <i18n.Text value="_SOME_TEXT_"/>
                        </Label>
                    </div>
                </Section>
            </Section>
        );
    }
}

export default I18nSection;