import React from "react";
import PropTypes from "prop-types";
import config from "config";
import styles from "./styles";

import LoggerFactory from "darch/src/utils/logger";
import Pager from "darch/src/pager";

const Logger = new LoggerFactory("app");

/**
 * App component
 *
 * @class Component
 * @extends {React.Component}
 */
class App extends React.Component {
    static propTypes = {
        actions: PropTypes.shape({
            i18NInit: PropTypes.func.isRequired
        }).isRequired
    };

    state = {
        initialized: false
    };

    /**
     * React lifecycle hook
     *
     * @returns {void}
     */
    async componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter", config);

        const { actions } = this.props;

        try {
            await Promise.all([
                actions.i18NInit("en-us")
            ]);

            logger.info("all promises resolved");
        } catch (error) {
            logger.error("all promises error", error);
        }

        // Close the loader
        window.appReady = true;

        // If all resources are also ready, then remove loader.
        if (window.resourcesReady) {
            const loader = document.getElementById("loader-overlay");
            if (loader) {
                loader.style.display = "none";
            }

            window.appLoaded = true;
        }

        // Set initialized
        this.setState({ initialized: true });
    }

    /**
     * Renders the component
     *
     * @returns {element} - The component elements
     */
    render() {
        const { initialized } = this.state;

        if (!initialized) {
            return null;
        }

        return (
            <div className={styles.main}>
                {Pager.utils.renderRoutes(this.props, "app")}
            </div>
        );
    }
}

export default (props) => (
    <App {...props} />
);