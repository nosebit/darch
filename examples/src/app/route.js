import React from "react";
import Pager from "darch/src/pager";
import loadPage from "bundle-loader?lazy!./index";

module.exports = {
    id: "app",

    Page: (props) => (
        <Pager load={loadPage}>
            {(Page) => <Page {...props} />}
        </Pager>
    ),

    routes: [
        require("./home/route"),
        require("./i18n/route")
    ],

    loadPage
};
