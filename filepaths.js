const path = require("path");

module.exports = {
    examples: {
        dest: path.resolve(__dirname, "examples"),
        devDest: path.resolve(__dirname, "examples/preview"),

        src: {
            html: "./examples/src/index.pug",
            sitemap: "./examples/src/sitemap.xml",
            index_js: "./examples/src/index.js",
            js: [ "./examples/src/**/*.js" ],
            assets: [
                "./examples/src/assets/icons/**/*"
            ],
            i18n: [ "./examples/src/assets/i18n/**/*" ]
        },

        vendor: {
            css: [],
            assets: []
        }
    },

    lib: {
        dest: path.resolve(__dirname, "lib"),
        src: {
            js: [
                "./src/**/*.js"
            ]
        }
    }
};
