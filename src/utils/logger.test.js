import LoggerFactory from "./logger";

describe("LoggerFactory", () => {
    it("should call console.log on debug logging", () => {
        global.console = { log: jest.fn() };

        const Logger = new LoggerFactory("scope_1");
        const logger = Logger.create("scope_2");

        logger.debug("test");

        expect(global.console.log).toBeCalled();
    });

    it("should call console.log on info logging", () => {
        global.console = { log: jest.fn() };

        const Logger = new LoggerFactory("scope_1");
        const logger = Logger.create("scope_2");

        logger.info("test");

        expect(global.console.log).toBeCalled();
    });

    it("should call console.log on warn logging", () => {
        global.console = { log: jest.fn() };

        const Logger = new LoggerFactory("scope_1");
        const logger = Logger.create("scope_2");

        logger.warn("test");

        expect(global.console.log).toBeCalled();
    });

    it("should call console.log on error logging", () => {
        global.console = { log: jest.fn() };

        const Logger = new LoggerFactory("scope_1");
        const logger = Logger.create("scope_2");

        logger.error("test");

        expect(global.console.log).toBeCalled();
    });
});