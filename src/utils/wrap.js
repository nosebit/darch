import React from "react";

/**
 * This function returns a wrapper component around a specified component.
 * 
 * @param {component} Component - The component to be wrapped.
 * @param {string} name - The name of the component.
 * @returns {componnet} - A wrapper component.
 */
export default function(Component, name) {
    return class extends React.Component {
        static displayName = name;

        /**
         * This function renders the HOC.
         * 
         * @returns {component} - The HOC.
         */
        render() {
            return (
                <Component {...this.props} />
            );
        }
    };
}