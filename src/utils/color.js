import vars from "../styles/variables";

/**
 * This function converts an hex color to rgb.
 *
 * @param {string} hex The hex color
 * @returns {{r: number, g: number, b: number}} A tuple with the color
 * codified in RGB.
 */
function hexToRGBColor(hex) {
    const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;

    hex = hex.replace(
        shorthandRegex,
        (m, r, g, b) => r + r + g + g + b + b
    );

    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    return result
        ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        }
        : { r: 255, g: 255, b: 255 };
}

/**
 * Get the hex color of a color name
 *
 * @param {string} color The name of the color
 * @returns {string} The hex value of the color, if supported. If not
 * supported, the name of the given color is returned.
 */
function getColor(color) {
    switch (color) {
        case "light":
            return vars.colorLight;
        case "dark":
            return vars.colorDark;
        case "positive":
            return vars.colorPositive;
        case "moody":
            return vars.colorMoody;
        case "calm":
            return vars.colorCalm;
        case "stable":
            return vars.colorStable;
        case "success":
            return vars.colorSuccess;
        case "warning":
            return vars.colorWarning;
        case "danger":
            return vars.colorDanger;
        default:
            return color;
    }
}

/**
 * This function checks if a color is valid.
 *
 * @param {string} color The hex color
 * @returns {boolean} True if valid. False otherwise.
 */
function isValidHexColor(color) {
    return (/^#?([0-9A-F]{6}|[0-9A-F]{3})$/i).test(color);
}

/**
 * This function check the brightness of a color.
 *
 * @param {string} color The hex color
 * @returns {number} How much darkness the color have
 */
function darkness(color) {
    if (!isValidHexColor(color)) {
        return 0;
    }

    let c = color.replace(/^#/, "");

    if (c.length === 3) {
        let c1 = "";

        for (let i = 0; i < c.length; i++) {
            c1 = `${c1}${c[i]}${c[i]}`;
        }

        c = c1;
    }

    const rgb = parseInt(c, 16); // convert rrggbb to decimal
    const r = (rgb >> 16) & 0xff; // extract red
    const g = (rgb >> 8) & 0xff; // extract green
    const b = (rgb >> 0) & 0xff; // extract blue

    const bright = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709;

    return Math.round((1 - bright / 255) * 100);
}

/**
 * Get the oposite color of the given color
 *
 * @param {string} color The hex color
 * @param {object} colors An object containing the dark and the light colors
 * @param {string} colors.lightColor The light color.
 * @param {string} colors.darkColor The dark color.
 * @returns {string} The dark or the light color
 */
function getOpositeColor(color, {
    lightColor = "white",
    darkColor = "black"
} = {}) {
    const ratio = darkness(getColor(color));

    if (ratio > 40) {
        return lightColor;
    }

    return darkColor;
}

/**
 * Transforms a color according to the given percentage. Retrieved from
 * https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
 *
 * @example
 * var color1 = "rgb(114,93,20,0.37423)";
 * var color2 = "#67DAF0";
 * var color3 = "#F3A";
 * 
 * c = shadeBlendConvert(0.3,color1); // color1 30% lighter
 * c = shadeBlendConvert(-0.13, color2); // color2 13% darker
 * c = shadeBlendConvert(0.42,color1,"c"); // color2 42% lighter and converted to #5fada177
 * 
 * @param {number} p Percentage for the color conversion.
 * @param {string} from The initial color.
 * @param {string} to The base color for conversion.
 * @returns {string} The transformed color.
 */
function shadeBlendConvert(p, from, to) {
    if (
        typeof (p) !== "number" || p < -1 || p > 1
        || (from[0] !== "r" && from[0] !== "#")
        || (typeof (to) !== "string" && typeof (to) !== "undefined")
    ) {
        return null; // TypeCheck and Validation
    }

    const sbcRip = (value) => {
        const l = value.length;
        const RGB = [];

        if (l > 9) {
            const d = value.split(",");

            if (d.length < 3 || d.length > 4) {
                return null; // ErrorCheck
            }

            RGB[0] = parseInt(d[0].slice(4));
            RGB[1] = parseInt(d[1]);
            RGB[2] = parseInt(d[2]);
            RGB[3] = d[3]
                ? parseFloat(d[3]) 
                : -1;
        } else {
            let d;
            if (l === 8 || l === 6 || l < 4) {
                return null; // ErrorCheck
            }

            if (l < 6) {
                d = "#" + value[1] + value[1]
                    + value[2] + value[2]
                    + value[3] + value[3]
                    + (l > 4
                        ? value[4] + "" + value[4]
                        : ""); // 3 digit
            }

            d = parseInt(value.slice(1), 16);
            RGB[0] = d >> 16 & 255;
            RGB[1] = d >> 8 & 255;
            RGB[2] = d & 255;
            RGB[3] = (l === 9 || l === 5)
                ? Math.round(((d >> 24 & 255) / 255) * 10000) / 10000
                : -1;
        }

        return RGB;
    };

    const isComplementColor = v => v === "c";

    const isHex = (from, to) => {

        const fromIsRGB = from.length > 9;
        const toIsRGB = typeof(to) === "string"
            ? to.length > 9 
                ? true 
                : to === "c"
                    ? !fromIsRGB
                    : false
            : fromIsRGB;

        return toIsRGB || (
            isComplementColor(to)
            && !fromIsRGB
        );
    };


    const b = p < 0;
    const resultIsRGB = isHex(from, to);

    // Convert Percent
    p = b 
        ? p * -1 
        : p;

    // Parse Complement Color
    to = (to && !isComplementColor(to))
        ? to
        : b
            ? "#000000"
            : "#FFFFFF";

    const f = sbcRip(from);
    const t = sbcRip(to);

    if (!f || !t) {
        return null; //ErrorCheck
    }

    if (resultIsRGB) {
        return "rgb("
            + Math.round((t[0] - f[0]) * p + f[0]) + "," 
            + Math.round((t[1] - f[1]) * p + f[1]) + ","
            + Math.round((t[2] - f[2]) * p + f[2])
            + (f[3] < 0 && t[3] < 0
                ? ")"
                : "," + (
                    (f[3] > -1 && t[3] > -1)
                        ? Math.round(((t[3] - f[3]) * p + f[3]) * 10000) / 10000
                        : t[3] < 0
                            ? f[3]
                            : t[3]
                ) + ")");
    } else {
        return "#" + (0x100000000
            + (f[3] > -1 && t[3] > -1
                ? Math.round(((t[3] - f[3]) * p + f[3]) * 255)
                : t[3] > -1
                    ? Math.round(t[3] * 255)
                    : f[3] > -1
                        ? Math.round(f[3] * 255)
                        : 255)
            * 0x1000000
            + Math.round((t[0] - f[0]) * p + f[0]) * 0x10000
            + Math.round((t[1] - f[1]) * p + f[1]) * 0x100
            + Math.round((t[2] - f[2]) * p + f[2])
        ).toString(16).slice(f[3] > -1 || t[3] > -1 
            ? 1 
            : 3);
    }
}

export {
    hexToRGBColor,
    getColor,
    isValidHexColor,
    darkness,
    getOpositeColor,
    shadeBlendConvert
};