import lodash from "lodash";
import vars from "../styles/variables";

const screenSizes = lodash.mapValues(
    lodash.pick(vars, [
        "screenTabletMin",
        "screenLaptopMin",
        "screenDesktopMin",
        "screenPhoneMax",
        "screenTabletMax",
        "screenLaptopMax"
    ]),
    v => parseInt(v)
);

/**
 * Determines the category of the screen based on the device window width.
 *
 * @param {number} width The screen width.
 * @returns {string} The screen category.
 */
function screenForWindowWidth(width) {
    if (width <= screenSizes.screenPhoneMax) {
        return "phone";
    } else if (
        screenSizes.screenTabletMin <= width 
        && width < screenSizes.screenTabletMax
    ) {
        return "tablet";
    } else if (
        screenSizes.screenLaptopMin <= width 
        && width < screenSizes.screenLaptopMax
    ) {
        return "laptop";
    }

    return "desktop";
}

/**
 * This function returns the text size relative to base text size and
 * rounded to prevent subpixel rendering issues.
 *
 * @static
 * @param {number} [scale=1] The scale for the text.
 * @param {number} [textSize=parseInt(vars.textSize)] The base for the size
 * @returns {number} The size based on the default size.
 * @memberof Style
 */
function getSize(
    scale = 1,
    textSize = parseInt(vars.textSize)
) {
    return Math.round(scale * textSize);
}

/**
 * This function append the base unit to a size.
 *
 * @param {number} [size=vars.textSize] The size for the unit.
 * @param {string} [baseUnit=vars.baseUnit] The unit to be applied on the size.
 * @returns {string} The string with the size followed by the base unit.
 */
function setUnit(
    size = parseInt(vars.textSize),
    baseUnit = vars.baseUnit
) {
    return `${size}${baseUnit}`;
}

export {
    screenForWindowWidth,
    getSize,
    setUnit
};