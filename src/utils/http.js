import qs from "qs";
import axios from "axios";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("http");

/**
 * This class defines the basic http interface to comunicate with remote server.
 */
class Http {
    /**
     * This function creates a new http instance.
     * 
     * @param {object} params - A map of params.
     * @param {string} params.baseUrl - A base url.
     * @param {bool} params.shared - A flag indicating if this instance should be shared.
     * @param {object} params.headers - A map of headers to add to request.
     */
    constructor({
        baseUrl = null,
        shared = false,
        headers = {}
    } = {}) {
        const logger = Logger.create("constructor");
        logger.debug("enter", {
            baseUrl, shared, headers
        });

        if (shared) {
            Http.shared = this;
        }

        this.baseUrl = baseUrl
            ? baseUrl.replace(/^(.+)\/$/, "$1")
            : null;

        this.headers = headers;
    }

    /**
     * This function sets a header key value.
     * 
     * @param {string} key - The header key.
     * @param {string} value - The header value associated with the key.
     * @returns {void}
     */
    setHeader(key, value) {
        this.headers[key] = value;
    }

    /**
     * This function performs a http request.
     * 
     * @param {string} method - The http request method verb.
     * @param {string} url - The request url.
     * @param {object} data - The request payload data.
     * @param {object} opts - A map of options.
     * @returns {Promise} A promise that gets resolved when server responds.
     */
    request(method = "GET", url, data, {
        headers = {}
    } = {}) {
        const logger = Logger.create("request");
        logger.debug("enter", {
            method, url, data, headers
        });

        const reqOpts = {
            url: this.baseUrl
                ? `${this._baseUrl}/${url.replace(/^\/(.+)$/, "$1")}`
                : url,
            headers: Object.assign({}, this.headers, headers),
            method: method.toUpperCase(),
            paramsSerializer: function (params) {
                return qs.stringify(params);
            }
        };

        if (data) {
            if (reqOpts.method === "GET") {
                reqOpts.params = data;
            } else {
                reqOpts.data = data;
            }
        }

        // Send request through axios
        return axios(reqOpts)
            .then((response) => {
                logger.debug("response", response);
                return response.data;
            });
    }
}

export default Http;