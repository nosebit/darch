/**
 * @namespace Utils
 */

import moment from "moment";
import lodash from "lodash";

/**
 * This function retrieve the color function for some log level.
 * 
 * @param {string} level - The log level.
 * @returns {function} A function that maps text to color.
 */
function getLevelColor(level) {
    switch (level) {
        case "debug": return "grey";
        case "info": return "blue";
        case "warn": return "orange";
        case "error": return "red";
    }

    return (text) => text;
}

const levelOrder = [
    "debug", "info", "warn", "error"
];

/**
 * This class defines a logger factory which are attached to a first level scope like a 
 * module.
 * 
 * @class
 * @memberof Utils
 * @example
 * let Logger = new LoggerFactory("myModule")
 * 
 * function myFunction() {
 *      let logger = Logger.create("myFunction");
 *      logger.info("hello, i'm inside myFunction of module myModule");
 * }
 */
class LoggerFactory {
    static level = process.env.NODE_ENV !== "production"
        ? 0
        : 1;

    /**
     * This function is the main logging method.
     * 
     * @param {string} level - The log level.
     * @param {string} message - The log message.
     * @param {object} data - Meta data to be printed with log message.
     * @param {object} opts - Options for the log method.
     * @returns {void}
     */
    static _log(
        level,
        message,
        data,
        opts = {}
    ) {
        const levelColor = getLevelColor(level);

        let dataStr = "";

        if (data) {
            dataStr = lodash.isString(data)
                ? `: ${data}`
                : ": " + JSON.stringify(data);
        }

        let text = `%c${level}: %c${message}`;

        if (level === "log") {
            text = message;
        }

        text = `${text} %c${dataStr}`;
        const timestamp = moment();
        const symbol = level !== "log"
            ? (opts.symbol || "- ")
            : "";

        console.log(// eslint-disable-line
            `${symbol}${text}`,
            `color: ${levelColor}`,
            "color: black",
            "color: grey"
        );

        return {
            text,
            symbol,
            timestamp,
            levelColor,
            print: function () {
                console.log( // eslint-disable-line
                    `${this.symbol}${this.text}`,
                    `color: ${this.levelColor}`,
                    "color: black",
                    "color: grey"
                );
            },
            elapsed: function () {
                return moment().diff(this.timestamp, "milliseconds");
            }
        };
    }

    /**
     * This function logs raw message.
     * 
     * @param {string} message - The log message.
     * @param {object} data - Meta data to be printed with log message.
     * @param {object} opts - Options for the log method.
     * @returns {void}
     */
    static log(
        message,
        data,
        opts
    ) {
        return LoggerFactory._log(
            "log",
            message,
            data,
            opts
        );
    }

    /**
     * This function logs debug message.
     * 
     * @param {string} message - The log message.
     * @param {object} data - Meta data to be printed with log message.
     * @param {object} opts - Options for the log method.
     * @returns {void}
     */
    static debug(
        message,
        data,
        opts
    ) {
        return LoggerFactory._log(
            "debug",
            message,
            data,
            opts
        );
    }

    /**
     * This function logs info message.
     * 
     * @param {string} message - The log message.
     * @param {object} data - Meta data to be printed with log message.
     * @param {object} opts - Options for the log method.
     * @returns {void}
     */
    static info(
        message,
        data,
        opts
    ) {
        return LoggerFactory._log(
            "info",
            message,
            data,
            opts
        );
    }

    /**
     * This function logs warn message.
     * 
     * @param {string} message - The log message.
     * @param {object} data - Meta data to be printed with log message.
     * @param {object} opts - Options for the log method.
     * @returns {void}
     */
    static warn(
        message,
        data,
        opts
    ) {
        return LoggerFactory._log(
            "warn",
            message,
            data,
            opts
        );
    }

    /**
     * This function logs error message.
     * 
     * @param {string} message - The log message.
     * @param {object} data - Meta data to be printed with log message.
     * @param {object} opts - Options for the log method.
     * @returns {void}
     */
    static error(
        message,
        data,
        opts
    ) {
        return LoggerFactory._log(
            "error",
            message,
            data,
            opts
        );
    }

    /**
     * This function creates a new logger factory.
     * 
     * @param {string} moduleName - The first level scope of logging.
     * @param {object} opts - Options for the logger.
     */
    constructor(
        moduleName,
        opts
    ) {
        this.moduleName = moduleName;
        this.opts = lodash.merge(
            {
                level: LoggerFactory.level,
                disabled: LoggerFactory.disabled
            },
            opts
        );
    }

    /**
     * This function parses a secret value for logging, i.e., return the own value when not running in production mode.
     * 
     * @param {string} value - The log message.
     * @returns {void}
     */
    secret(value) {
        return process.env.NODE_ENV !== "production"
            ? value
            : undefined;
    }

    /**
     * This function creates a new logger instance within the factory 
     * first level scope.
     * 
     * @param {string} scopeName - The second level scope of logging.
     * @returns {Logger} A new logger instance.
     */
    create(scopeName) {
        return new Logger({ // eslint-disable-line
            moduleName: this.moduleName,
            scopeName
        }, this.opts);
    }
}

/**
 * This class defines a logger which are attached to a second level scope like a 
 * function.
 * 
 * @class
 * @memberof Utils
 * @private
 */
class Logger {
    // Static properties
    static consoleLevelMap = {
        "debug": "log",
        "info": "info",
        "warn": "warn",
        "error": "error"
    };

    /**
     * This function creates a new logger instance.
     * 
     * @param {object} params - Params wrapper
     * @param {string} params.moduleName - The wrapping module name that logger gonna be part of.
     * @param {string} params.scopeName - The specific scope for this logger.
     * @param {object} opts - Options for the logger.
     */
    constructor({
        moduleName = "global",
        scopeName = ""
    } = {}, opts = {}) {
        this.moduleName = moduleName;
        this.scopeName = scopeName;
        this.opts = opts;
        this.level = process.env.ENV !== "production"
            ? "debug"
            : "info";
    }

    /**
     * This function performs the main logging through console and is
     * used by all other logging functions. 
     * 
     * @private
     * 
     * @param {string} level - The log level which can be debug, info, warning or error.
     * @param {string} message - The message to be logged.
     * @param {object} data - Data to be logged with the message.
     * @param {object} opts - An array with metadata entities to be logged alongside the log message.
     * @returns {void}
     */
    _log(
        level,
        message,
        data,
        opts = {}
    ) {
        const { disabled, level: _level } = this.opts;

        if (disabled || _level >= 0 && levelOrder.indexOf(level) < _level) {
            return;
        }

        const levelColor = getLevelColor(level);
        let dataStr = "";

        if (data) {
            dataStr = lodash.isString(data)
                ? `: ${data}`
                : ": " + JSON.stringify(data);
        }

        let text = `%c${level}`;
        text = `${text}: %c[${this.moduleName}] ${this.scopeName}`;
        text = `${text} : ${message} %c${dataStr}`;

        const timestamp = moment();
        const symbol = opts.symbol || "- ";

        console.log( // eslint-disable-line
            `${symbol}${text}`,
            `color: ${levelColor}`,
            "color: black",
            "color: grey"
        );

        return {
            text,
            symbol,
            timestamp,
            levelColor,
            print: function () {
                console.log( // eslint-disable-line
                    `${this.symbol}${this.text}`,
                    `color: ${this.levelColor}`,
                    "color: black",
                    "color: grey"
                );
            },
            elapsed: function () {
                const currentMoment = moment();
                return currentMoment.diff(this.timestamp, "milliseconds");
            }
        };
    }

    /**
     * This function performs a debug log (with importance level of 0).
     * 
     * @param {string} message - The message to be logged.
     * @param {object} data - Data to be logged with the message.
     * @param {object} opts - An array with metadata entities to be logged alongside the log message.
     * @returns {void}
     */
    debug(
        message,
        data,
        opts
    ) {
        this._log(
            "debug",
            message,
            data,
            opts
        );
    }

    /**
     * This function performs a info log (with importance level of 1).
     * 
     * @param {string} message - The message to be logged.
     * @param {object} data - Data to be logged with the message.
     * @param {object} opts - An array with metadata entities to be logged alongside the log message.
     * @returns {void}
     */
    info(
        message,
        data,
        opts
    ) {
        this._log(
            "info",
            message,
            data,
            opts
        );
    }

    /**
     * This function performs a warning log (with importance level of 2).
     * 
     * @param {string} message - The message to be logged.
     * @param {object} data - Data to be logged with the message.
     * @param {object} opts - An array with metadata entities to be logged alongside the log message.
     * @returns {void}
     */
    warn(
        message,
        data,
        opts
    ) {
        this._log(
            "warn",
            message,
            data,
            opts
        );
    }

    /**
     * This function performs a error log (with importance level of 3).
     * 
     * @param {string} message - The message to be logged.
     * @param {object} data - Data to be logged with the message.
     * @param {object} opts - An array with metadata entities to be logged alongside the log message.
     * @returns {void}
     */
    error(
        message,
        data,
        opts
    ) {
        this._log(
            "error",
            message,
            data,
            opts
        );
    }
}

// Exports
export default LoggerFactory;