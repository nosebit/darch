import promiseMiddleware from "redux-promise-middleware";
import { createStore, combineReducers, applyMiddleware } from "redux";
import lodash from "lodash";

/**
 * This class defines a redux manager.
 */
class Redux {
    /**
     * This function uses the shared redux instance to dispatch an action.
     * 
     * @param {object} action - A redux action.
     * @returns {void}
     */
    static dispatch(action) {
        if (!Redux.shared) {
            throw "no shared instance available";
        }

        return Redux.shared.store.dispatch(action);
    }

    /**
     * This function gets the redux global state.
     * 
     * @returns {object} The global state.
     */
    static getState() {
        if (!Redux.shared) {
            throw "no shared instance available";
        }

        return Redux.shared.store.getState();
    }

    /**
     * This function creates a new redux manager instance.
     * 
     * @param {array} reducers - List of reducers to combine.
     * @param {object} opts - Map of options.
     * @param {bool} opts.shared - Flag indicating if this instance should be a shared one.
     * @param {array} opts.middlewares - A list of middlewares to be applied to redux store.
     * @param {array} opts.promiseTypeSuffixes - A list of promises sufixes for action state machine.
     */
    constructor(reducers = {}, opts = {}) {
        opts = lodash.merge({
            shared: false,
            middlewares: [],
            promiseTypeSuffixes: [
                "PENDING",
                "COMPLETED",
                "FAILED"
            ]
        }, opts);

        // Set shared instance
        if (opts.shared) {
            Redux.shared = this;
        }

        opts.middlewares.push(promiseMiddleware({
            promiseTypeSuffixes: opts.promiseTypeSuffixes
        }));

        this.store = createStore(
            combineReducers(reducers),
            applyMiddleware(...opts.middlewares)
        );
    }
}

export default Redux;
