import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("container.component");

/**
 * Main component class.
 */
class ContainerCmp extends React.Component {
    static propTypes = {
        size: PropTypes.oneOf([
            "sm",
            "md"
        ])
    };

    static defaultProps = {};

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const { theme } = this.props;

        const classes = [
            theme.container,
            this.props.className,
            this.props.size
                ? theme[`container-${this.props.size}`]
                : ""
        ];

        return (
            <div className={classNames(classes)}>
                {this.props.children}
            </div>
        );
    }
}

export default ContainerCmp;