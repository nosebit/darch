import { themr } from "react-css-themr";
import Component from "./component";
import styles from "./styles";
import wrap from "darch/src/utils/wrap";

let Composed;

Composed = themr(
    "Container",
    styles
)(Component);

// Final wrap.
Composed = wrap(Composed, "ContainerCmp");

// export
export default Composed;