import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Link } from "react-router-dom";
import * as Color from "darch/src/utils/color";
import * as Screen from "darch/src/utils/screen";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("label");

/**
 * Main component class.
 */
class Label extends React.Component {
    static propTypes = {
        scale: PropTypes.number,
        layout: PropTypes.oneOf([
            "flat",
            "outline"
        ]),
        onClick: PropTypes.func,
        to: PropTypes.string,
        theme: PropTypes.object
    };

    static defaultProps = {
        scale: 1,
        layout: "flat",
        color: "moody"
    };

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const { scale, layout, theme } = this.props;
        const color = Color.getColor(this.props.color);

        const classes = [
            theme.label,
            theme[`label-${layout}`],
            this.props.onClick
                ? theme.clickable
                : ""
        ];

        const style = {
            fontSize: Screen.setUnit(Screen.getSize(scale))
        };

        switch (layout) {
            case "flat": {
                style.background = color;
                style.borderColor = this.props.borderColor || color;
                style.color = (
                    Color.darkness(color) > 50
                        ? "white"
                        : "black"
                );

                break;
            }
            case "outline": {
                style.borderColor = color;
                style.color = color;
                break;
            }
        }

        if (this.props.to) {
            return (
                <Link
                    to={this.props.to}
                    className={classNames(classes)}
                    style={style}
                >
                    {this.props.children}
                </Link>
            );
        }

        return (
            <span
                className={classNames(classes)}
                style={style}
                onClick={this.props.onClick}
            >
                {this.props.children}
            </span>
        );
    }
}

export default Label;