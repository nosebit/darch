import React from "react";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("headline");

/**
 * Main component class.
 */
class Headline extends React.Component {
    static propTypes = {};
    static defaultProps = {};

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const { theme } = this.props;

        return (
            <div className={theme.headlineCmpCmp}>
                {this.props.children}
            </div>
        );
    }
}

export default Headline;