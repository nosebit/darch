import { themr } from "react-css-themr";
import Component from "./component";
import styles from "./styles";
import wrap from "darch/src/utils/wrap";

let Composed;

Composed = themr(
    "ProgressBar",
    styles
)(Component);

// Final wrap.
Composed = wrap(Composed, "ProgressBarCmp");

// export
export default Composed;