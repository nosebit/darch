import React from "react";
import PropTypes from "prop-types";
import LoggerFactory from "darch/src/utils/logger";
import Screen from "darch/src/utils/screen";

const Logger = new LoggerFactory("progress.bar");

/**
 * Main component class.
 */
class ProgressBar extends React.Component {
    static propTypes = {
        rate: PropTypes.number.isRequired,
        strokeWidth: PropTypes.number,
        widthScale: PropTypes.number,
        heightScale: PropTypes.number,
        animateOnMount: PropTypes.bool,
        strokeColor: PropTypes.string,
        strokeLinecap: PropTypes.oneOf([
            "butt", "round", "square"
        ]),
        trailColor: PropTypes.string
    };

    static defaultProps = {
        rate: 0,
        strokeWidth: 0.5,
        scale: 1,
        animateOnMount: true,
        strokeColor: "#3e98c7",
        strokeLinecap: "round",
        trailColor: "#dedede"
    };

    /**
     * This function creates a new instance of this component.
     * 
     * @param {object} props - The component props.
     */
    constructor(props) {
        super(props);

        this.state = {
            rate: props.rate
        };
    }

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.deug("enter");

        if (this.props.animateOnMount) {
            this.animationTimer = setTimeout(() => {
                this.requestAnimationFrame
                    = window.requestAnimationFrame(() => {
                        this.setState({
                            rate: this.props.rate
                        });
                    });
            }, 0);
        }
    }

    /**
     * This function is called when this component gets it's props/state
     * updated.
     * 
     * @param {object} prevProps - The previous props for this component.
     * @returns {void}
     */
    componentDidUpdate(prevProps) {
        if (this.props.rate !== prevProps.rate) {
            this.setState({ rate: this.props.rate });
        }
    }

    /**
     * This function is called when this component is about the get
     * unmounted from the DOM.
     * 
     * @returns {void}
     */
    componentWillUnmount() {
        clearTimeout(this.animationTimer);
        window.cancelAnimationFrame(this.requestAnimationFrame);
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const logger = Logger.create("render");
        logger.debug("enter");

        const { scale, strokeLinecap, theme } = this.props;
        const { rate } = this.state;
        const size = Screen.getSize(scale);
        const strokeWidth = Screen.getSize(this.props.strokeWidth);
        const linecap = strokeLinecap !== "butt"
            ? strokeWidth / 2
            : 0;
        const left = linecap;
        const trailRight = size - linecap;
        const pathRight = size * (rate / 100) - linecap;

        const sizeWithUnit = Screen.setUnit(size);
        const dashOffsetWithunit = Screen.setUnit(size * (100 - rate) / 100);

        logger.debug("data", {
            rate,
            size,
            strokeWidth,
            linecap,
            left,
            trailRight,
            pathRight,
            sizeWithUnit,
            dashOffsetWithunit
        });

        const trailStyle = {
            stroke: this.props.trailColor,
            strokeLinecap: this.props.strokeLinecap
        };

        const pathStyle = {
            strokeDasharray: `${sizeWithUnit}`,
            strokeDashoffset: `${dashOffsetWithunit}`,
            stroke: this.props.strokeColor,
            strokeLinecap: this.props.strokeLinecap
        };

        return (
            <svg className={theme.progress}
                width={size}
                height={strokeWidth}>
                <path className={theme.trail}
                    d={`M ${left},${strokeWidth / 2} L ${trailRight},${strokeWidth / 2}`} //eslint-disable-line
                    strokeWidth={strokeWidth}
                    fillOpacity={0}
                    style={trailStyle} />

                <path className={theme.path}
                    d={`M ${left},${strokeWidth / 2} L ${pathRight},${strokeWidth / 2}`} //eslint-disable-line
                    strokeWidth={strokeWidth}
                    fillOpacity={0}
                    style={pathStyle}
                />
            </svg>
        );
    }
}

export default ProgressBar;