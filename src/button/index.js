import { themr } from "react-css-themr";
import Component from "./component";
import styles from "./styles";
import wrap from "darch/src/utils/wrap";

let Composed;

Composed = themr(
    "Button",
    styles
)(Component);

// Final wrap.
Composed = wrap(Composed, "ButtonCmp");

// export
export default Composed;