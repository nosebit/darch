import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Link } from "react-router-dom";
import LoggerFactory from "darch/src/utils/logger";
import * as Color from "darch/src/utils/color";
import * as Screen from "darch/src/utils/screen";
import i18n from "darch/src/i18n";

const Logger = new LoggerFactory("button");

/**
 * Main component class.
 */
class Button extends React.Component {
    static propTypes = {
        scale: PropTypes.number,
        layout: PropTypes.oneOf([
            "flat",
            "outline"
        ]),
        color: PropTypes.string,
        type: PropTypes.oneOf([
            "button",
            "reset",
            "submit"
        ]),
        textCase: PropTypes.oneOf([
            "lower",
            "upper"
        ]),
        loading: PropTypes.bool,
        active: PropTypes.bool,
        disabled: PropTypes.bool,
        block: PropTypes.bool,
        onClick: PropTypes.func,
        onFocus: PropTypes.func,
        onBlur: PropTypes.func
    };

    static defaultProps = {
        scale: 1,
        layout: "flat",
        color: "moody",
        type: "button",
        textCase: "upper",
        loading: false,
        active: false,
        disabled: false,
        block: false,
        onClick: () => { },
        onFocus: () => { },
        onBlur: () => { },
        loadingComponent: (<i18n.Text value="_LOADING_" />)
    };

    // Instance properties
    state = {};

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * This function handles mouse enter event.
     * 
     * @returns {void}
     */
    onMouseEnter() {
        this.setState({
            focused: true
        });
    }

    /**
     * This function handles mouse leave event.
     * 
     * @returns {void}
     */
    onMouseLeave() {
        this.setState({
            focused: false
        });
    }

    /**
     * This function gets the base color for the button.
     * 
     * @returns {string} - The color.
     */
    getColor() {
        const color = Color.getColor(this.props.color);

        if (this.state.focused) {
            return Color.shadeBlendConvert(-0.1, color); // Color 10% darker.
        }

        return color;
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const logger = Logger.create("render");
        logger.debug("enter", {
            disabled: this.props.disabled
        });

        const {
            scale,
            layout,
            disabled,
            block,
            active,
            loading,
            textCase,
            theme
        } = this.props;

        const color = this.getColor();

        const classes = [
            theme.button,
            theme[`button-text-case-${textCase}`],
            theme[`button-${layout}`],
            block
                ? theme["button-block"]
                : "",
            disabled
                ? theme[`button-${layout}-disabled`]
                : "",
            active
                ? theme[`button-${layout}-active`]
                : "",
            loading
                ? theme[`button-${layout}-disabled`]
                : ""
        ];

        const style = {
            fontSize: Screen.setUnit(Screen.getSize(scale))
        };

        switch (layout) {
            case "flat": {
                style.background = color;
                style.borderColor = this.props.borderColor || color;
                style.color = (
                    Color.darkness(color) > 50
                        ? "white"
                        : "black"
                );

                break;
            }
            case "outline": {
                style.borderColor = color;
                style.color = color;
                break;
            }
        }

        if (this.props.to) {
            return (
                <Link className={classNames(classes)}
                    style={style}
                    to={this.props.to}>

                    {this.props.loading
                        ? this.props.loadingComponent
                        : this.props.children}
                </Link>
            );
        }

        return (
            <button className={classNames(classes)}
                style={style}
                type={this.props.type}
                onClick={this.props.onClick}
                onFocus={this.props.onFocus}
                onBlur={this.props.onBlur}
                onMouseEnter={this.onMouseEnter}
                onMouseLeave={this.onMouseLeave}
            >
                {this.props.loading
                    ? this.props.loadingComponent
                    : this.props.children}
            </button>
        );
    }
}

export default Button;