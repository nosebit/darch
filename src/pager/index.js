import React from "react";

/**
 * This component deals with async page loading (code splitting).
 */
class Pager extends React.Component {
    static utils = require("./utils");

    state = {
        mod: null
    }

    /**
     * This function is called when this component is about to get mounted
     * into the DOM.
     * 
     * @returns {void}
     */
    componentWillMount() {
        this.load(this.props);
    }

    /**
     * This function is called when this component going to receive new
     * props.
     * 
     * @param {object} nextProps - The new props.
     * @returns {void}
     */
    componentWillReceiveProps(nextProps) {
        if (nextProps.load !== this.props.load) {
            this.load(nextProps);
        }
    }

    /**
     * This function loads the page.
     * 
     * @param {object} props - The page loader props.
     * @returns {void}
     */
    load(props) {
        this.setState({ mod: null });

        props.load((mod) => {
            this.setState({
                // handle both es imports and cjs
                mod: mod.default 
                    ? mod.default 
                    : mod
            });
        });
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        return this.state.mod
            ? this.props.children(this.state.mod) 
            : null;
    }
}

export default Pager;