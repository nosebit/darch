import React from "react";
import lodash from "lodash";
import { Route, Switch } from "react-router-dom";
import i18n from "darch/src/i18n";

/**
 * This function preload routes.
 * 
 * @param {object} spec - Router spec map.
 * @param {array} spec.routes - The list of routes for the current router.
 * @returns {void}
 */
function preloadRoutes(spec = {}) {
    const { routes } = spec;

    if (!routes || !routes.length) {
        return;
    }

    for (const route of routes) {
        route.loadPage(() => { });
    }
}

/**
 * This function render a router routes.
 * 
 * @param {object} spec - The router spec map.
 * @param {array} spec.routes - The list of routes for the current router.
 * @returns {void}
 */
function renderRoutes(spec = {}) {
    const { routes } = spec;

    if (!routes || !routes.length) {
        return;
    }

    return (
        <Switch>
            {lodash.map(lodash.orderBy(routes, [ (RouteSpec) => (
                `${(i18n.utils.routePath(RouteSpec.id) || "")}`
            ) ], [ "desc" ]), (RouteSpec, i) => {
                const path = `${(i18n.utils.routePath(RouteSpec.id) || "")}`;

                return (
                    <Route key={i}
                        path={path}
                        render={(props) => (
                            <RouteSpec.Page
                                {...props}
                                routes={RouteSpec.routes}
                            />
                        )}>
                    </Route>
                );
            })}
        </Switch>
    );
}

export {
    preloadRoutes,
    renderRoutes
};