import lodash from "lodash";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("validators");
const emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i; //eslint-disable-line

const Validators = {
    $required: {
        name: "$required", 
        validate: (value) => {
            const logger = Logger.create("required");
            logger.debug("enter", { value });

            return !(!lodash.isNumber(value) && lodash.isEmpty(value));
        }
    },

    $email: {
        name: "$email",
        validate: (value) => {
            const logger = Logger.create("email");
            logger.debug("enter", { value });

            if (!value) {
                return true;
            }

            return emailRegex.test(value);
        }
    },

    $alphanumeric: {
        name: "$alphanumeric",
        validate: (value) => {
            const logger = Logger.create("alphanumeric");
            logger.info("enter", { value });

            return (/^[a-zA-Z0-9]*$/).test(value);
        }
    },

    $digits: {
        name: "$digits",
        validate: (value) => {
            const logger = Logger.create("digits");
            logger.debug("enter", { value });

            return (/^[0-9]*$/).test(value);
        }
    },

    $lowercase: {
        name: "$lowercase",
        validate: (value) => {
            const logger = Logger.create("lowercase");
            logger.info("enter", { value });

            return (/^[a-z0-9]*$/).test(value);
        }
    },

    $uppercase: {
        name: "$uppercase",
        validate: (value) => {
            const logger = Logger.create("uppercase");
            logger.info("enter", { value });

            return (/^[A-Z0-9]*$/).test(value);
        }
    },

    $domain: {
        name: "$domain",
        validate: (value) => {
            const logger = Logger.create("domain");
            logger.info("enter", { value });

            return (/^[a-zA-Z0-9_]+$/).test(value);
        }
    },

    $equal: {
        name: "$equal",
        watch: [ 0 ],   // opts indexes of watch field names
        validate: (value, opts=[], context={}) => {
            const logger = Logger.create("equal");
            logger.info("enter", { value,opts,context });

            const values = context.values||{};

            if (opts.length
            && !lodash.isEqual(value, values[opts[0]])) {
                return false;
            }

            return true;
        }
    }
};

/**
 * This is the main entrypoint for validating by a validator name.
 * 
 * @param {string} name - The name of the validator to lead the validation.
 * @param {any} value - The value to be validated.
 * @param {object} opts - A map of options to be passed to validator.
 * @param {object} context - A map with context data.
 * @param {object} context.values - The values of all fields within the form (for cross validation).
 * @returns {Promise} - A promise which resolves to true if value is valid and false otherwise.
 */
function validate(name, value, opts, context) {
    const logger = Logger.create("validate");
    logger.info("enter", { name, value, opts });

    if (Validators[name]) {
        return Promise.resolve(Validators[name].validate(value, opts, context));
    }

    return Promise.resolve(true);
}

// Export
export {
    Validators,
    validate
};