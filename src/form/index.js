import { Validators } from "./validators";
import Component from "./component";

/**
 * This function registers a new form validator.
 * 
 * @param {object} validator - The validator spec object.
 * @returns {void}
 */
function registerValidator(validator) {
    if (!validator || !validator.name) {
        return;
    }

    Validators[validator.name] = validator;
}

// Set static stuff.
Component.registerValidator = registerValidator;

// Export
export default Component;