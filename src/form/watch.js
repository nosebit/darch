import lodash from "lodash";
import { Validators } from "./validators";

/**
 * This function process all watchers associated to an object validator.
 * 
 * @param {object} validator - The validator object spec.
 * @param {component} field - The field which value we want to validate.
 * @param {object} fieldWatchers - A map which value is a list of all fields that are watching for a field with key name.
 * @param {*} watchingFields - A list of all fields watched by the validator.
 * @returns {void}
 */
function processObjectValidator(
    validator, 
    field,
    fieldWatchers = {}, 
    watchingFields = []
) {
    if (!validator.watch) {
        return;
    }

    const validatorWatch = lodash.flatten([ validator.watch ]);
    const watchedFieldNames = [];

    for (const watch of validatorWatch) {
        if (lodash.isString(watch)) {
            watchedFieldNames.push(watch);
        }
    }

    for (const fieldName of watchedFieldNames) {
        fieldWatchers[fieldName] = fieldWatchers[fieldName]||[];
        fieldWatchers[fieldName].push(field.props.name);
        watchingFields.push(fieldName);
    }
}

/**
 * This function process all watchers associated to a named validator.
 * 
 * @param {string} validator - The validator name in the form name:opt_1,...,opt_k.
 * @param {component} field - The field which value we want to validate.
 * @param {object} fieldWatchers - A map which value is a list of all fields that are watching for a field with key name.
 * @param {*} watchingFields - A list of all fields watched by the validator.
 * @returns {void}
 */
function processStringValidator(
    validator, 
    field,
    fieldWatchers = {}, 
    watchingFields = []
) {
    const opts = validator.split(":");
    const validatorName = opts.shift();
    const watchedFieldNames = [];
    let validatorWatch = lodash.get(
        Validators,
        `${validatorName}.watch`
    );

    if (!lodash.isUndefined(validatorWatch)) {
        validatorWatch = lodash.flatten([ validatorWatch ]);

        for (const watch of validatorWatch) {
            // If watch is number, then it represents the index
            // of opts that contains the watched field name.
            if (lodash.isNumber(watch)) {
                if (watch >= 0 && watch < opts.length) {
                    watchedFieldNames.push(opts[watch]);
                }
            // If watch is string, then it is the watched field
            // name.
            } else if (lodash.isString(watch)) {
                watchedFieldNames.push(watch);
            }
        }
    }

    for (const fieldName of watchedFieldNames) {
        fieldWatchers[fieldName] = fieldWatchers[fieldName]||[];
        fieldWatchers[fieldName].push(field.props.name);
        watchingFields.push(fieldName);
    }
}

/**
 * This function is the main entrypoint for watching.
 * 
 * @param {component} field - The target field.
 * @param {object} fieldWatchers - A map listing all fields that are watching the target field.
 * @returns {void}
 */
function processWatchers(field, fieldWatchers={}) {
    const watchingFields = [];
    let validators = field.props.validators;

    if (lodash.isString(validators)) {
        validators = validators.split("|");

        // Process field validators that require watch
        // other fields.
        for (const validator of validators) {
            processStringValidator(
                validator,
                field,
                fieldWatchers,
                watchingFields
            );
        }
    } else if (lodash.isArray(validators)) {
        for (const validator of validators) {
            if (lodash.isString(validator)) {
                processStringValidator(
                    validator,
                    field,
                    fieldWatchers,
                    watchingFields
                );
            } else if (lodash.isObject(validator)) {
                processObjectValidator(
                    validator,
                    field,
                    fieldWatchers,
                    watchingFields
                );
            }
        }
    }

    field.watchingFields = watchingFields;
}

// Export
export default processWatchers;