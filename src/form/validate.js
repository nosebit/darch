import lodash from "lodash";
import { Validators } from "./validators";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("form.validate");

/**
 * This function runs an object validator.
 * 
 * @param {object} validator - An object specifying the validator.
 * @param {string} validator.name - The validator name id.
 * @param {string|array<string>} validator.on - A list of events that triggers the validator to run.
 * @param {component} field - The fiels where the validator should run.
 * @param {object} values - The values of all fields within the form (for watchers).
 * @param {object} args - Arguments map.
 * @param {string} args.event - The event name that triggered the validation to run.
 * @param {string} args.validateOn - A global event that triggers validations (validator.on has preference over this one).
 * @returns {Promise} - A promise which gets resolved to true with field value is valid or false otherwise.
 */
function evalObjectValidator(validator, field, values, args) {
    const logger = Logger.create("evalObjectValidator");
    logger.debug("enter", { validator, values, args });

    const context = { values };
    const { name } = validator;
    let promise;

    const on = validator.on
        ? lodash.flatten([ validator.on ])
        : null;

    // Use own validator on event to perform
    // validation or inherits from form validateOn.
    if (
        (args.event
            && on
            && on.indexOf(args.event) < 0)
        || (args.event
            && !on
            && args.validateOn !== args.event)
    ) {
        logger.debug("skiping validator", { name });

        promise = Promise.resolve({
            name,
            valid: field.state.isValidMap[name]
        });
    } else {
        logger.debug("running validator", { name });

        // Call field onValidationStart to notify clients
        // that this validation is going to start.
        field.props.onValidationStart(name);

        promise = Promise.resolve(validator.validate(
            field.state.value,
            args.opts,
            context
        )).then((value) => {
            logger.debug(`validator ${name} value`, { value });

            let result = {
                name,
                valid: false
            };

            // Process validation value.
            if (lodash.isBoolean(value)) {
                result.valid = value;
            } else if (lodash.isObject(value)) {
                result = lodash.assign({}, result, value);
            }

            // Call field onValidationEnd to notify clients
            // that this validation ended.
            field.props.onValidationEnd(name, result);

            return result;
            
        }).catch((error) => {
            logger.error("validator error", error);
            return {
                name,
                valid: false
            };
        });
    }

    return promise;
}

/**
 * This function runs a validator by it's name.
 * 
 * @param {string} validator - The validator name id like registered in validators map.
 * @param {component} field - The fiels where the validator should run.
 * @param {object} values - The values of all fields within the form (for watchers).
 * @param {object} args - Arguments map.
 * @param {string} args.event - The event name that triggered the validation to run.
 * @param {string} args.validateOn - A global event that triggers validations (validator.on has preference over this one).
 * @returns {Promise} - A promise which gets resolved to true with field value is valid or false otherwise.
 */
function evalStringValidator(validator, field, values, args) {
    const logger = Logger.create("evalStringValidator");
    logger.debug("enter", { validator });

    const opts = validator.split(":");
    const name = opts.shift();

    logger.debug("validator is string", {
        validator,
        name,
        opts
    });

    // Get validator spec from validators list.
    return Validators[name]
        ? evalObjectValidator(
            Validators[name], 
            field, 
            values, 
            Object.assign({}, args, { opts })
        )
        : Promise.resolve(true);
}

/**
 * This function run all field validators when specified as an array of
 * validators names or validator object spec.
 * 
 * @param {component} field - The field to be validated.
 * @param {object} values - The values of all fields within the form (for watchers).
 * @param {object} opts - A map of options.
 * @returns {Promise} - A promise which gets resolved to an object with shape {isValidMap: object, validationResults: object}
 * where isValidMap is a map which key is the validator name and value is a boolean indicating if field value is valid/invalid and 
 * validationResults is a map which key is the validator name and value is an object encapsulating validation result more detailed info.
 */
function validateFromArray(field, values, opts) {
    const logger = Logger.create("validateFromArray");

    logger.debug("enter", {
        name: field.props.name,
        validators: field.props.validators,
        values: values,
        event: opts.event,
        validateOn: opts.validateOn
    });

    const promises = [];

    for (const validator of field.props.validators) {
        if (lodash.isString(validator)) {
            promises.push(evalStringValidator(
                validator,
                field,
                values,
                opts
            ));
        } else if (lodash.isObject(validator)) {
            const { name } = validator;

            if (!lodash.isFunction(validator.validate)
            || lodash.isEmpty(name)) {
                continue;
            }

            promises.push(evalObjectValidator(
                validator,
                field,
                values,
                opts
            ));
        }
    }

    return Promise.all(promises).then((results) => {
        const map = lodash.reduce(results, (map, result) => {
            logger.debug("isValidMap : reduce", { map, result });

            map.isValidMap[result.name] = result.valid;
            map.validationResults[result.name] = result;
            
            return map;
        }, { isValidMap: {}, validationResults: {} });

        logger.debug("isValidMap", { results, map });

        return map;
    });
}


/**
 * This function run all field validators when specified as string with
 * shape "name_1:opts_1_1,...opts_1_k|...|name_n:opts_n_1,...opts_n_r".
 * 
 * @param {component} field - The field to be validated.
 * @param {object} values - The values of all fields within the form (for watchers).
 * @param {object} opts - A map of options.
 * @returns {Promise} - A promise which gets resolved to an object with shape {isValidMap: object, validationResults: object}
 * where isValidMap is a map which key is the validator name and value is a boolean indicating if field value is valid/invalid and 
 * validationResults is a map which key is the validator name and value is an object encapsulating validation result more detailed info.
 */
function validateFromString(field, values, opts) {
    const logger = Logger.create("validateFromString");

    logger.debug("enter", {
        name: field.props.name,
        validators: field.props.validators,
        values: values,
        event: opts.event,
        validateOn: opts.validateOn
    });

    const validators = field.props.validators.split("|");
    const promises = [];

    lodash.forEach(validators, (validator, idx) => {
        logger.debug(`validator ${idx}`, { validator });

        promises.push(evalStringValidator(
            validator,
            field,
            values,
            opts
        ));
    });

    return Promise.all(promises).then((results) => {
        const map = lodash.reduce(results, (map, result) => {
            logger.debug("isValidMap : reduce", { map, result });

            map.isValidMap[result.name] = result.valid;
            map.validationResults[result.name] = result;

            return map;
        }, { isValidMap: {}, validationResults: {} });

        logger.debug("isValidMap", { results, map });

        return map;
    });
}

/**
 * This function is the main entrypoint for validating a field.
 * 
 * @param {component} field - The field to be validated.
 * @param {object} values - The values of all fields within the form (for watchers).
 * @param {object} opts - A map of options.
 * @returns {Promise} - A promise which gets resolved to an object with shape {isValidMap: object, validationResults: object}
 * where isValidMap is a map which key is the validator name and value is a boolean indicating if field value is valid/invalid and
 * validationResults is a map which key is the validator name and value is an object encapsulating validation result more detailed info.
 */
function validate(field, values, opts = {}) {
    const logger = Logger.create("validate");
    
    logger.debug("enter", {
        name: field.props.name,
        validators: field.props.validators,
        values
    });

    if (lodash.isString(field.props.validators)) {
        return validateFromString(field, values, opts);
    } else if (lodash.isArray(field.props.validators)) {
        return validateFromArray(field, values, opts);
    }

    return Promise.resolve({});
}

// Export
export default validate;