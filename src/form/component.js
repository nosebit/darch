import React from "react";
import PropTypes from "prop-types";
import lodash from "lodash";
import validate from "./validate";
import watch from "./watch";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("form");

/**
 * Main component class.
 */
class Form extends React.Component {
    static propTypes = {
        validateOn: PropTypes.oneOf([
            "change",
            "blur",
            "submit"
        ]),
        showErrorsOn: PropTypes.oneOf([
            "change",
            "blur",
            "submit"
        ]),
        clearErrorOnBecomeValid: PropTypes.bool,
        disableSubmitButtonOnErrors: PropTypes.bool,
        disableSubmitButtonOnValidating: PropTypes.bool,
        loading: PropTypes.bool,
        onSubmit: PropTypes.func
    };

    static defaultProps = {
        validateOn: "change",
        showErrorsOn: "blur",
        clearErrorOnBecomeValid: true,
        disableSubmitButtonOnErrors: true,
        disableSubmitButtonOnValidating: true,
        loading: false,
        onSubmit: () => Promise.resolve(),
        onChange: () => { }
    };

    /**
     * This function constructs a new instance of this component.
     * 
     * @param {object} props - The component props.
     */
    constructor(props) {
        super(props);

        // Instance properties
        this.fields = {};
        this.fieldErrors = {};
        this.fieldWatchers = {};
        this.values = {};
        this.originalValues = {};
        this.submitCount = 0;
        this.state = {
            isValid: true
        };
    }

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * This function process all field children.
     * 
     * @param {object} children - The component children.
     * @returns {object} - The children processed.
     */
    processChildren(children) {
        if (!children) {
            return;
        }

        const logger = Logger.create("processChildren");
        logger.debug("enter");

        return React.Children.map(children, (component, idx) => {
            const displayName = lodash.get(component, "type.displayName");
            const oldChildren = lodash.get(component, "props.children");
            const newProps = {};
            let newChildren;

            logger.debug(`children map : ${idx}`, { type: displayName });

            switch (displayName) {
                case "FieldBase":
                case "FieldErrorCmp":
                case "ButtonCmp":
                    newProps.form = this;
                    break;
                default:
            }

            // Set disabled for submit button when form is invalid.
            if (displayName === "ButtonCmp"
                && component.props.type === "submit") {
                newProps.loading = this.props.loading;

                logger.debug("found submit button", {
                    formIsValid: this.state.isValid
                });

                if ((this.props.disableSubmitButtonOnErrors
                    && !this.state.isValid) 
                || (this.props.disableSubmitButtonOnValidating
                    && this.state.validating)
                ) {
                    newProps.disabled = true;
                }
                if (this.props.disableSubmitButtonOnErrors) {
                    newProps.disabled = !this.state.isValid;
                }
            }

            // Process old children
            if (oldChildren) {
                newChildren = this.processChildren(oldChildren);
            }

            // Clone component with new props and children
            return newChildren || lodash.size(newProps)
                ? React.cloneElement(component, newProps, newChildren)
                : component;
        });
    }

    /**
     * This function add a field to fields list.
     * 
     * @param {component} field - The field to be added to this form.
     * @returns {void}
     */
    addField(field) {
        const logger = Logger.create("addField");
        
        logger.debug("enter", {
            name: field.props.name,
            value: field.state.value
        });

        this.fields[field.props.name] = field;
        this.values[field.props.name] = field.state.value;
        this.originalValues[field.props.name] = field.state.value;

        // Extract watch fields.
        watch(field, this.fieldWatchers);

        // Validate field.
        if (!field.props.preventValidateOnMount) {
            this.validateField(field);
        }
    }

    /**
     * This function removes a field from fields list.
     * 
     * @param {component} field - The field to be removed from this form.
     * @returns {void}
     */
    removeField(field) {
        const logger = Logger.create("removeField");
        logger.debug("enter", {
            name: field.props.name
        });

        // Remove field from all watching fields.
        for (const fieldName of field.watchingFields) {
            lodash.pull(this.fieldWatchers[fieldName], field.props.name);
        }

        delete this.fields[field.props.name];
        delete this.values[field.props.name];
    }

    /**
     * This function add an field error component.
     * 
     * @param {component} errorCmp - The error component to be added.
     * @returns {void}
     */
    addFieldError(errorCmp) {
        const logger = Logger.create("addFieldError");
        logger.debug("enter", {
            for: errorCmp.props.for,
            validator: errorCmp.props.validator
        });

        const fieldErrors = this.fieldErrors[errorCmp.props.for] || {};
        fieldErrors[errorCmp.props.validator] = errorCmp;
        this.fieldErrors[errorCmp.props.for] = fieldErrors;
    }

    /**
     * This function removes an field error component.
     * 
     * @param {component} errorCmp - The error component to be removed from this form.
     * @returns {void}
     */
    removeFieldError(errorCmp) {
        const logger = Logger.create("removeFieldError");
        logger.debug("enter", {
            for: errorCmp.props.for,
            validator: errorCmp.props.validator
        });

        const fieldErrors = this.fieldErrors[errorCmp.props.for] || {};
        delete fieldErrors[errorCmp.props.validator];
    }

    /**
     * This function validates a field value and updates the valid state of this form.
     * 
     * @param {component} field - The field to be validated.
     * @param {object} opts - A map of options.
     * @returns {Promise} - A promise which is resolved to true if the field is valid or false otherwise.
     */
    validateField(field, opts = {}) {
        const logger = Logger.create(`validateField : ${field.props.name}`);
        logger.debug("enter");

        this.setState({ validating: true });
        field.setState({ validating: true });

        return validate(
            field,
            this.values,
            lodash.assign({}, opts, {
                validateOn: this.props.validateOn
            })
        ).then((map) => {
            const { isValidMap, validationResults } = map;

            logger.debug("validate completed", {
                isValidMap: isValidMap,
                validationResults: validationResults,
                stateIsValidMap: field.state.isValidMap
            });

            // Get list of validators the become valid and a list of
            // those that become invalid.
            const becomeValid = [];
            const becomeInvalid = [];
            const invalids = [];
            const valids = [];

            logger.debug("isValidMap keys", {
                keys: lodash.keys(isValidMap)
            });

            // Get the list of validators that become valid.
            for (const name of lodash.keys(isValidMap)) {
                logger.debug("isValidMap key", {
                    key: name
                });

                if (
                    (lodash.isUndefined(field.state.isValidMap[name]) 
                        || !field.state.isValidMap[name])
                    && isValidMap[name]
                ) {
                    becomeValid.push(name);
                } else if (
                    (lodash.isUndefined(field.state.isValidMap[name]) 
                        || field.state.isValidMap[name])
                    && !isValidMap[name]
                ) {
                    becomeInvalid.push(name);
                }

                if (isValidMap[name]) {
                    valids.push(name);
                } else {
                    invalids.push(name);
                }
            }

            logger.debug("become valid or invalid", {
                becomeValid, becomeInvalid
            });

            return new Promise((resolve) => {
                this.setState({ validating: false });

                field.setState({
                    isValidMap,
                    validationResults,
                    validating: false
                }, () => {

                    // Clear errors.
                    (becomeValid.length && this.props.clearErrorOnBecomeValid
                        ? this.updateFieldErrors(field, { only: becomeValid })
                        : Promise.resolve()
                    ).then(() => {
                        const keys = Object.keys(validationResults || {});

                        // Let's iterate over validation results to check
                        // wether any result has a $set attribute.
                        for (const name of keys) {
                            const result = validationResults[name];

                            if (lodash.isObject(result.$set)) {
                                const keys = Object.keys(result.$set || {});

                                // Iterate over each set field.
                                for (const fieldName of keys) {
                                    const field = this.fields[fieldName];
                                    this.onFieldChange(
                                        field, 
                                        result.$set[fieldName]
                                    );
                                }
                            }
                        }

                        resolve({
                            becomeValid,
                            becomeInvalid
                        });
                    });
                });
            }).then(() => (
                // Validate form.
                this.validateForm()
            ));
        });
    }

    /**
     * This function validates all fields in the form.
     * 
     * @returns {Promise} - A promise which is resolved when the form gets validated.
     */
    validateForm() {
        const logger = Logger.create("validateForm");
        logger.debug("enter");

        for (const fieldName of lodash.keys(this.fields)) {
            logger.debug("fieldName", { fieldName });

            const field = this.fields[fieldName];

            if (!field.state.isValidMap) {
                logger.debug("does not have isValidMap");
                continue;
            }

            const keys = lodash.keys(field.state.isValidMap);

            for (const name of keys) {
                if (!field.state.isValidMap[name]) {
                    logger.debug("isValidMap false field", {
                        isValidMap: field.state.isValidMap,
                        isValidMapForItem: field.state.isValidMap[name]
                    });

                    return new Promise((resolve) => {
                        this.setState({ isValid: false }, resolve);
                    });
                }
            }
        }

        return new Promise((resolve) => {
            this.setState({ isValid: true }, resolve);
        });
    }

    /**
     * This function updates field error components.
     * 
     * @param {component} field - The field which errors we want to update.
     * @param {object} opts - A map of options.
     * @returns {void}
     */
    updateFieldErrors(field, opts = {}) {
        const logger = Logger.create("updateFieldErrors");
        logger.debug("enter", { name: field.props.name });

        const isValidMap = field.state.isValidMap;
        const fieldErrors = this.fieldErrors[field.props.name] || {};
        const promises = [];

        logger.debug("isValidMaps", {
            isValidMap: field.state.isValidMap
        });

        for (const name of lodash.keys(isValidMap)) {
            logger.debug("isValidMap iteration", {
                name,
                valid: isValidMap[name]
            });

            if (
                !fieldErrors[name]
                || (opts.only && opts.only.indexOf(name) < 0) 
                || (
                    // Prevent if the current event does not
                    // match the showOn prop of fieldError or the
                    // form showErrorsOn.
                    opts.event
                    && (
                        (fieldErrors[name].props.showOn
                            && fieldErrors[name].props.showOn !== opts.event)
                        || (!fieldErrors[name].props.showOn
                            && this.props.showErrorsOn !== opts.event)
                    )
                )
            ) {
                continue;
            }

            promises.push(new Promise((resolve) => {
                fieldErrors[name].setState({
                    active: !isValidMap[name]
                }, resolve);
            }));
        }

        return Promise.all(promises).then((results) => {
            logger.debug("all components updated");
            return results;
        });
    }

    /**
     * This function handles field value change.
     * 
     * @param {component} field - The field which value got changed.
     * @param {any} value - The new value of the field.
     * @returns {void}
     */
    onFieldChange(field, value) {
        const logger = Logger.create(`onFieldChange : ${field.props.name}`);

        logger.debug("enter", {
            value, hideErrorsOn: this.props.hideErrorsOn
        });

        const { values } = this;

        values[field.props.name] = value;

        return new Promise((resolve) => {
            field.setState({ value }, () => {
                Promise.resolve()
                    .then(() => (
                        this.validateField(
                            field,
                            { event: "change" }
                        )
                    ))
                    .then(() => (
                        this.updateFieldErrors(
                            field,
                            { event: "change" }
                        )
                    ))
                    .then(() => {
                        const keys = this.fieldWatchers[field.props.name] || [];

                        // Validate all watchers.
                        for (const fieldName of keys) {
                            logger.debug(
                                "validate watching field",
                                { fieldName }
                            );

                            this.validateField(this.fields[fieldName]);
                        }
                    })
                    .then(() => {
                        // Call on change
                        this.props.onChange(values);

                        resolve({
                            value,
                            isValidMap: field.state.isValidMap,
                            validationResults: field.state.validationResults
                        });
                    });
            });
        });
    }

    /**
     * This function handles field blur.
     * 
     * @param {component} field - The field which became blured.
     * @returns {Promise} - A promise that gets resolved when everything is done.
     */
    onFieldBlur(field) {
        const logger = Logger.create(`onFieldBlur : ${field.props.name}`);
        logger.debug("enter");

        field.blurCount = field.blurCount || 0;
        field.blurCount++;

        return Promise.resolve()
            .then(() => (
                this.validateField(field, { event: "blur" })
            ))
            .then(() => (
                this.updateFieldErrors(field, { event: "blur" })
            ))
            .then(() => {
                const data = {
                    value: this.values[field.props.name],
                    isValidMap: field.state.isValidMap,
                    validationResults: field.state.validationResults
                };

                return data;
            });
    }

    /**
     * This function handles field focus.
     * 
     * @param {component} field - The field which became focused.
     * @returns {Promise} - A promise that gets resolved when everything is done.
     */
    onFieldFocus(field) {
        const logger = Logger.create(`onFieldFocus : ${field.props.name}`);
        logger.debug("enter");

        return Promise.resolve({
            value: this.values[field.props.name],
            isValidMap: field.state.isValidMap,
            validationResults: field.state.validationResults
        });
    }

    /**
     * This function handles submit event.
     * 
     * @param {object} evt - The event object.
     * @returns {void}
     */
    async onSubmit(evt) {
        const logger = Logger.create("onSubmit");
        logger.debug("enter");

        evt.preventDefault();

        // Prevent submit when form is invalid or validating.
        if (!this.state.isValid || this.state.validating) {
            return;
        }

        // Build updated field names.
        const updatedFields = [];

        for (const fieldName of Object.keys(this.values)) {
            const equal = lodash.isEqual(
                this.values[fieldName],
                this.originalValues[fieldName]
            );

            if (!equal) {
                updatedFields.push(fieldName);
            }
        }

        const submitResult = this.props.onSubmit(
            lodash.cloneDeep(this.values), 
            {
                name: this.props.name,
                updatedFields
            }
        );

        const resetFieldValues = await Promise.resolve(submitResult);

        if (lodash.isObject(resetFieldValues)) {
            this.values = lodash.assign(
                {},
                this.values,
                resetFieldValues
            );

            // Set state for fields.
            lodash.forOwn(resetFieldValues, (value, fieldName) => {
                this.fields[fieldName].setState({ value }, () => {
                    this.validateField(this.fields[fieldName])
                        .then(() => {
                            lodash.forOwn(
                                this.fieldErrors[fieldName], 
                                (fieldError) => {
                                    fieldError.setState({ active: false });
                                }
                            );
                        });
                });
            });
        }

        this.originalValues = lodash.cloneDeep(this.values);
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        return (
            <form noValidate
                onSubmit={this.onSubmit}>
                {this.processChildren(this.props.children)}
            </form>
        );
    }
}


export default Form;