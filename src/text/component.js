import React from "react";
import PropTypes from "prop-types";
import LoggerFactory from "darch/src/utils/logger";
import Color from "darch/src/utils/color";

const Logger = new LoggerFactory("text");

/**
 * Main component class.
 */
class Text extends React.Component {
    static propTypes = {
        scale: PropTypes.number,
        block: PropTypes.bool,
        width: PropTypes.string,
        color: PropTypes.string
    };

    static defaultProps = {
        scale: 1,
        block: false
    };

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const { scale, block, color } = this.props;

        const style = {
            fontSize: `${scale}em`,
            display: block
                ? "block"
                : "inline-block",
            width: this.props.width,
            color: color
                ? Color.getColor(color) 
                : undefined,
            verticalAlign: "middle"
        };

        return (
            <div style={style}>
                {this.props.children}
            </div>
        );
    }
}

export default Text;