import { themr } from "react-css-themr";
import Component from "./component";
import styles from "./styles";
import wrap from "darch/src/utils/wrap";

let Composed;

Composed = themr(
    "Code",
    styles
)(Component);

// Final wrap.
Composed = wrap(Composed, "CodeCmp");

// export
export default Composed;