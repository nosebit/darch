import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Prism from "prismjs";

import LoggerFactory from "darch/src/utils/logger";

import "prism-themes/themes/prism-atom-dark";

const Logger = new LoggerFactory("code");

/**
 * This component deals with code rendering.
 */
class Code extends React.Component {
    static propTypes = {
        lang: PropTypes.string
    };

    static defaultProps = {
        lang: "text"
    };

    /**
     * This function is called when this component is about to get mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentWillMount() {
        const logger = Logger.create("componentWillMount");
        logger.info("enter");

        const { lang } = this.props;

        if (lang && !Prism.languages[lang]) {
            this.loadLang(lang);
        }
    }

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.info("enter");
    }

    /**
     * This function is called when this component is about to receive 
     * new props.
     * 
     * @param {object} nextProps - The new props.
     * @returns {void}
     */
    componentWillReceiveProps(nextProps) {
        const { lang } = nextProps;

        if (lang && !Prism.languages[lang]) {
            this.loadLang(lang);
        }
    }

    /**
     * This function loads a new language.
     * 
     * @param {string} lang - The language to be loaded.
     * @returns {void}
     */
    loadLang(lang) {
        if (!lang) {
            return;
        }

        require(`prismjs/components/prism-${lang}`);
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const { lang, theme, value } = this.props;

        if (!Prism.languages[lang]) {
            return null;
        }

        return (
            <div
                className={theme.container}
            >
                <pre className={classNames([
                    theme.pre,
                    `language-${lang}`
                ])}>
                    <code
                        className={classNames([
                            "prism",
                            theme.code
                        ])}
                        ref={(ref) => {
                            this.codeBlockRef = ref;
                        }}
                        dangerouslySetInnerHTML={{
                            __html: Prism.highlight(
                                value,
                                Prism.languages[lang]
                            )
                        }}
                    />
                </pre>
            </div>
        );
    }
}

export default Code;