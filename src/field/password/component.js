import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import LoggerFactory from "darch/src/utils/logger";
import i18n from "darch/src/i18n";
import Progress from "darch/src/progress";

const Logger = new LoggerFactory("field.password");

/**
 * Main component class.
 */
class FieldPassword extends React.Component {
    static propTypes = {
        value: PropTypes.string,
        allowUnmask: PropTypes.bool,
        unmaskLabel: PropTypes.string,
        maskLabel: PropTypes.string,
        showStrength: PropTypes.bool,
        scale: PropTypes.number,
        maskChar: PropTypes.string,
        maskTimeout: PropTypes.number,
        evalStrength: PropTypes.func,
        fieldRef: PropTypes.func,
        focus: PropTypes.bool
    };

    static defaultProps = {
        value: "",
        allowUnmask: true,
        unmaskLabel: "UNMASK",
        maskLabel: "MASK",
        showStrength: true,
        scale: 1,
        maskChar: "•",
        maskTimeout: 1000,
        fieldRef: () => { },
        focus: false
    };

    state = {
        inputValue: "",
        unmasked: false,
        strength: 0
    }

    /**
     * This function creates a new instance of this component.
     * 
     * @param {object} props - The component props.
     */
    constructor(props) {
        super(props);
    }

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * This function is called when this component gets it's props/state
     * updated.
     * 
     * @param {object} prevProps - The previous props for this component.
     * @returns {void}
     */
    componentDidUpdate(prevProps) {
        if (this.state.unmasked
            && this.props.value !== prevProps.value
            && this.selection) {
            let pos = this.selection.start + 1;

            if ((this.isDelete || this.isBackspace)
                && this.selection.originalOffset === 0) {
                pos = this.selection.start;
            }

            this.isDelete = false;
            this.isBackspace = false;

            // Force selection range right position.
            this.inputRef.setSelectionRange(pos, pos);
        }
    }

    /**
     * This function handles change in masked input.
     * 
     * @param {object} evt - The event object.
     * @returns {void}
     */
    onInputChange(evt) {
        const logger = Logger.create("onChange");
        logger.debug("enter", {
            value: evt.target.value
        });

        const { value, maskChar } = this.props;
        const { unmasked } = this.state;
        const valueChars = value.split("");
        const oldInputValue = unmasked
            ? this.props.value
            : this.state.inputValue;
        const oldInputChars = oldInputValue.split("");
        const newInputValue = evt.target.value;
        const newInputChars = newInputValue.split("");

        // Clear maskTimer, because we gonna mask right now.
        clearTimeout(this.maskTimer);

        // Dead keys
        if (!this.selection) {
            if (unmasked) {
                return this.props.onChange(evt.target.value);
            }

            return this.setState({
                inputValue: evt.target.value
            });
            // First of all, deal with removing chars.
        } else if (this.selection.start !== this.selection.end) {
            oldInputChars.splice(
                this.selection.start,
                this.selection.end - this.selection.start
            );

            valueChars.splice(
                this.selection.start,
                this.selection.end - this.selection.start
            );

            logger.debug("selection is range", {
                oldInputChars,
                valueChars
            });
        }

        // Mask all non mask chars of new input that are also
        // in old input.
        for (let j = 0, i = 0; i < newInputChars.length; i++) {
            if (newInputChars[i] === maskChar) {
                j++;
                continue;
            }

            // old char
            if (newInputChars[i] === oldInputChars[j]) {
                newInputChars[i] = maskChar;
                j++;
                // new char : should be added to valueChars.
            } else {
                valueChars.splice(i, 0, newInputChars[i]);
            }
        }

        logger.debug("data", {
            selection: this.selection,
            valueChars, oldInputChars, newInputChars
        });

        const newValue = valueChars.join("");

        this.setState({
            inputValue: newInputChars.join(""),
            strength: this.evalStrength(newValue)
        }, () => {
            this.props.onChange(newValue);

            let pos = this.selection.start + 1;

            if ((this.isDelete || this.isBackspace)
                && this.selection.originalOffset === 0) {
                pos = this.selection.start;
            }

            // Reset values.
            this.isDelete = false;
            this.isBackspace = false;

            // Force selection range right position.
            this.inputRef.setSelectionRange(pos, pos);

            // Mask everything later
            this.maskTimer = setTimeout(() => {
                let { inputValue } = this.state;

                inputValue = inputValue.replace(
                    new RegExp(`[^${maskChar}]`, "g"),
                    maskChar
                );

                this.setState({ inputValue }, () => {
                    // @TODO : Correct a bug with updating position when user
                    // has already moved the cursor between insertion moment
                    // and this remask moment.

                    // Force selection range right position.
                    this.inputRef.setSelectionRange(pos, pos);
                });
            }, this.props.maskTimeout);
        });
    }

    /**
     * This function handles input key down.
     * 
     * @param {object} evt - The object event.
     * @returns {void}
     */
    onInputKeyDown(evt) {
        const logger = Logger.create("onKeyDown");
        const { keyCode, key, keyIdentifier } = evt;

        logger.debug("enter", { keyCode, key, keyIdentifier });

        // Check for dead key (Chrome and Firefox set key or keyIdentifier
        // as "Dead", while Safari set it as "Unidentified"... Fuck IE)
        if ([ "Dead", "Unidentified" ].indexOf(key || keyIdentifier) >= 0) {
            logger.debug("dead key");
            this.selection = null;
            return;
        }

        this.selection = {
            start: this.inputRef.selectionStart,
            end: this.inputRef.selectionEnd,
            originalOffset: (
                this.inputRef.selectionEnd
                - this.inputRef.selectionStart
            )
        };

        switch (keyCode) {
            // Backspace
            case 8:
                this.selection.start -= 1;
                this.isBackspace = true;
                break;
            // Delete
            case 46:
                this.selection.end += 1;
                this.isDelete = true;
                break;
            default:
        }
    }

    /**
     * This function evaluates the strenght of the password.
     * 
     * @param {string} password - The password.
     * @return {number} - The password strength in percent.
     */
    evalStrength(password) {
        const logger = Logger.create("evalStrength");
        logger.info("enter", { password });

        let strength = 0;

        if (this.props.evalStrength) {
            strength = this.props.evalStrength(password);
        } else {
            let points = 0;

            if ((/[a-z]/g).test(password)) {
                points++;
            }

            if ((/[A-Z]/g).test(password)) {
                points++;
            }

            if ((/[0-9]/g).test(password)) {
                points++;
            }

            if ((/[$-/:-?{-~!"^_`[\]]/g).test(password)) {
                points++;
            }

            strength = (points / 4) * 100;
        }

        logger.debug("leave", { strength });

        return strength;
    }

    /**
     * This function toggles the password mask.
     * 
     * @returns {void}
     */
    toggleMask() {
        const newState = {
            unmasked: !this.state.unmasked
        };

        if (!newState.unmasked) {
            const inputChars = Array((this.props.value || "").length)
                .fill(this.props.maskChar);

            newState.inputValue = inputChars.join("");
        }

        this.setState(newState, () => {
            // Focus input again.
            this.inputRef.focus();
        });
    }

    /**
     * This function handles mouse down on control.
     * 
     * @returns {void}
     */
    onControlMouseDown() {
        this.inputRef.focus();
    }

    /**
     * This function handles input focus event.
     * 
     * @returns {void}
     */
    onInputFocus() {
        this.setState({
            isInputFocused: true
        }, () => {
            this.props.onFocus();
        });
    }

    /**
     * This function handles input blur event.
     * 
     * @returns {void}
     */
    onInputBlur() {
        this.setState({
            isInputFocused: false
        }, () => {
            this.props.onBlur();
        });
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const {
            spec,
            placeholder,
            scale,
            isValid,
            unmaskLabel,
            maskLabel,
            theme
        } = this.props;

        const {
            isInputFocused,
            unmasked
        } = this.state;

        const controlClasses = [
            theme.control,
            !isValid
                ? theme.invalid
                : "",
            isInputFocused
                ? theme.active
                : ""
        ];

        return (
            <div onMouseDown={this.onControlMouseDown}
                className={classNames(controlClasses)}
                style={{ fontSize: `${scale}em` }}
            >
                <div className={theme.inputZone}>
                    <input {...{
                        value: unmasked
                            ? this.props.value
                            : this.state.inputValue,
                        onKeyDown: this.onInputKeyDown,
                        onChange: this.onInputChange,
                        onBlur: this.onInputBlur,
                        onFocus: this.onInputFocus,
                        placeholder: i18n.utils.translate({
                            spec,
                            value: placeholder
                        })
                    }}
                    ref={(ref) => {
                        this.inputRef = ref;
                        this.props.fieldRef(ref);

                        if (ref && this.props.focus) {
                            ref.focus();
                        }
                    }} />
                </div>

                {this.props.showStrength
                    ? (
                        <div className={theme.strengthZone}>
                            <div className={theme.container}>
                                <Progress.Bar scale={3}
                                    strokeWidth={0.5}
                                    rate={this.state.strength}
                                    strokeColor="#26A65B"
                                    strokeLinecap="square" />
                            </div>
                        </div>
                    )
                    : null
                }

                {this.props.allowUnmask
                    ? (
                        <div className={theme.unmaskButtonZone}>
                            <div
                                className={theme.unmaskButton}
                                onClick={this.toggleMask}
                            >
                                <i18n.Text
                                    value={unmasked
                                        ? maskLabel
                                        : unmaskLabel}
                                />
                            </div>
                        </div>
                    )
                    : null
                }
            </div>
        );
    }
}

export default FieldPassword;