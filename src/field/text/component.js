import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import InputMask from "react-input-mask";
import { LoggerFactory } from "darch/src/utils";
import i18n from "darch/src/i18n";

const Logger = new LoggerFactory("field.text");

/**
 * Main component class.
 */
class FieldText extends React.Component {
    static propTypes = {
        scale: PropTypes.number,
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
        mask: PropTypes.string,
        maskChar: PropTypes.string,
        fieldRef: PropTypes.func,
        focus: PropTypes.bool
    };

    static defaultProps = {
        value: "",
        scale: 1,
        fieldRef: () => { },
        focus: false
    };

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");
    }

    /**
     * This function handles field value change.
     * 
     * @param {object} evt - The event object.
     * @returns {void}
     */
    onChange(evt) {
        const logger = Logger.create("onChange");
        logger.debug("enter", { value: evt.target.value });

        this.props.onChange(evt.target.value);
    }

    /**
     * This function handles key down.
     * 
     * @param {object} evt - The event object.
     * @returns {void}
     */
    onKeyDown(evt) {
        const logger = Logger.create("onKeyDown");
        logger.debug("enter", {
            keyCode: evt.keyCode,
            keyIdentifier: evt.keyIdentifier
        });
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const {
            spec,
            placeholder,
            scale,
            mask,
            maskChar,
            theme
        } = this.props;

        const props = {
            value: this.props.value,
            onKeyDown: this.onKeyDown,
            onChange: this.onChange,
            onBlur: this.props.onBlur,
            onFocus: this.props.onFocus,
            type: this.props.type,
            disabled: this.props.disabled,
            ref: (ref) => {
                this.props.fieldRef(ref);

                if (ref && this.props.focus) {
                    ref.focus();
                }
            },
            placeholder: i18n.utils.translate({
                spec, 
                value: placeholder
            })
        };

        const classes = [
            theme.field,
            this.props.disabled
                ? theme.disabled
                : "",
            this.props.validating
                ? theme.validating
                : "",
            !this.props.isValid
                ? theme.invalid
                : ""
        ];

        if (mask) {
            props.mask = mask;
            props.maskChar = maskChar;

            return (
                <InputMask
                    className={classNames(classes)}
                    style={{ fontSize: `${scale}em` }}
                    {...props}
                />
            );
        }

        return (
            <input
                className={classNames(classes)}
                style={{ fontSize: `${scale}em` }}
                {...props}
            />
        );
    }
}

export default FieldText;