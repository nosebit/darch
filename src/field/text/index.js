import { themr } from "react-css-themr";
import { connect } from "react-redux";
import Base from "../base";
import Component from "./component";
import styles from "./styles";

let Composed;

/**
 * Redux map state to props function.
 *
 * @param {object} state - The redux state
 * @param {object} ownProps - A list of props passed to the component.
 * @returns {object} The props the should be injected into the component.
 */
function mapStateToProps(state) {
    return {
        spec: state.i18n.spec
    };
}

/**
 * Redux dispatch to props map.
 * 
 * @returns {object} The props the should be injected into the component.
 */
function mapDispatchToProps() {
    return {};
}

// Compose with themr
Composed = themr(
    "FieldText",
    styles
)(Component);

// Compose with redux.
Composed = connect(
    mapStateToProps,
    mapDispatchToProps
)(Composed);

// Compose with base
Composed = Base(Composed);

// export
export default Composed;