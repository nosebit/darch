import React from "react";
import PropTypes from "prop-types";
import lodash from "lodash";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("field.base");

/**
 * This function create a compose component.
 * 
 * @param {component} Component - The component to compose.
 * @returns {component} - A composed component.
 */
function compose(Component) {
    /**
     * This is the composed component.
     */
    class FieldBase extends React.Component {
        static displayName = "FieldBase";
        
        static propTypes = {
            validators: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.arrayOf(PropTypes.oneOfType([
                    PropTypes.string,
                    PropTypes.shape({
                        name: PropTypes.string.isRequired,
                        on: PropTypes.oneOf([ "change","blur" ]),
                        watch: PropTypes.oneOfType([
                            PropTypes.string,
                            PropTypes.arrayOf(PropTypes.string)
                        ]),
                        validate: PropTypes.func.isRequired
                    })
                ]))
            ]),
            name: PropTypes.string.isRequired,
            isValidMap: PropTypes.object,
            onChange: PropTypes.func,
            onBlur: PropTypes.func,
            onFocus: PropTypes.func,
            size: PropTypes.number
        };

        static defaultProps = {
            validators: [],
            onChange: () => { },
            onBlur: () => { },
            onFocus: () => { },
            onValidationStart: () => { },
            onValidationEnd: () => { },
            size: 1
        };

        /**
         * This function constructs a new instance of the component.
         * 
         * @param {object} props - The component props.
         */
        constructor(props) {
            const logger = Logger.create("constructor");
            logger.debug("enter", {
                hasForm: !!props.form
            });

            super(props);

            this.state = {};

            // If this field got a parent form, then the parent
            // form gonna manage the field value.
            if (props.form) {
                this.state.value = props.value;
                this.state.isValidMap = {};
            }
        }

        /**
         * This function is called when this component gets mounted into
         * the DOM.
         * 
         * @returns {void}
         */
        componentDidMount() {
            const logger = Logger.create("componentDidMount");
            logger.debug("enter", {
                hasForm: !!this.props.form
            });

            if (this.props.form) {
                this.props.form.addField(this);
            }
        }

        /**
         * This function is called when this component props/state gets updated.
         * 
         * @param {object} prevProps - The previous props for this component.
         * @returns {void}
         */
        componentDidUpdate(prevProps) {
            const logger = Logger.create("componentDidUpdate");
            logger.debug("enter");

            // Prop value changed
            if (this.props.form 
            && !lodash.isEqual(this.props.value, prevProps.value)) {
                this.onChange(this.props.value);
            }
        }

        /**
         * This function is called when component is about the get unmounted from the DOM.
         * 
         * @returns {void}
         */
        componentWillUnmount() {
            const logger = Logger.create("componentWillUnmount");
            logger.debug("enter");

            if (this.props.form) {
                this.props.form.removeField(this);
            }
        }

        /**
         * This function handles field value change.
         * 
         * @param {any} value - The changed value.
         * @returns {void}
         */
        onChange(value) {
            const logger = Logger.create("onChange");
            logger.debug("enter", { value });

            // If there is a parent form, then parent form is responsible
            // form manage this field value.
            if (this.props.form) {
                this.props.form.onFieldChange(this, value).then((result) => {
                    this.props.onChange(
                        result.value, 
                        result.isValidMap,
                        result.validationResults
                    );
                });
            // Otherwise let parent component to manage the value.
            } else {
                this.props.onChange(value);
            }
        }

        /**
         * This function handles field blur.
         * 
         * @returns {void}
         */
        onBlur() {
            // Notify parent form that field got blured.
            if (this.props.form) {
                this.props.form.onFieldBlur(this).then((result) => {
                    this.props.onBlur(
                        result.value, 
                        result.isValidMap,
                        result.validationResults
                    );
                });
            } else {
                this.props.onBlur();
            }
        }

        /**
         * This function handles field focus.
         * 
         * @returns {void}
         */
        onFocus() {
            // Notify parent form that field got focused.
            if (this.props.form) {
                this.props.form.onFieldFocus(this).then((result) => {
                    this.props.onFocus(
                        result.value, 
                        result.isValidMap,
                        result.validationResults
                    );
                });
            } else {
                this.props.onFocus();
            }
        }

        /**
         * This function renders this component into the DOM.
         * 
         * @returns {element} The component main element.
         */
        render() {
            const value = this.props.form
                ? this.state.value
                : this.props.value;

            let isValid = true;
            const isValidMap = this.props.form
                ? this.state.isValidMap
                : this.props.isValidMap;

            // Evaluate isValid on base of isValidMap.
            for (const valid of lodash.values(isValidMap||{})) {
                if (!valid) {
                    isValid = false;
                    break;
                }
            }

            const props = lodash.assign({}, this.props, {
                value: value,
                onChange: this.onChange,
                onBlur: this.onBlur,
                onFocus: this.onFocus,
                isValid: isValid,
                size: this.props.size,
                validating: this.state.validating
            });

            return (
                <span>
                    <Component {...props} />
                </span>
            );
        }
    }

    return FieldBase;
}

export default compose;