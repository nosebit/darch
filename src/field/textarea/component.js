import React from "react";
import PropTypes from "prop-types";
import { findDOMNode } from "react-dom";
import classNames from "classnames";
import LoggerFactory from "darch/src/utils/logger";
import i18n from "darch/src/i18n";

const Logger = new LoggerFactory("field.textarea");

/**
 * Main component class.
 */
class FieldTextArea extends React.Component {
    static propTypes = {
        scale: PropTypes.number,
        value: PropTypes.string,
        resize: PropTypes.string,
        fieldRef: PropTypes.func
    };

    static defaultProps = {
        value: "",
        scale: 1,
        resize: "none",
        fieldRef: () => { }
    };

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");

        window.addEventListener("resize", this.onWindowResize);
        this.onWindowResize();
    }

    /**
     * This function is called when this component is about to get unmounted
     * from the DOM.
     * 
     * @returns {void}
     */
    componentWillUnmount() {
        window.removeEventListener("resize", this.onWindowResize);
    }

    /**
     * This function handles window resize.
     * 
     * @returns {void}
     */
    onWindowResize() {
        const elem = findDOMNode(this.textareaRef);

        elem.style.height = "auto";
        elem.style.height = `${elem.scrollHeight}px`;
    }

    /**
     * This function handle field value change.
     * 
     * @param {object} evt - The event object.
     * @returns {void}
     */
    onChange(evt) {
        const logger = Logger.create("onChange");
        logger.debug("enter", { value: evt.target.value });

        this.props.onChange(evt.target.value);
        this.onWindowResize();
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const { theme } = this.props;

        const props = {
            value: this.props.value,
            onChange: this.onChange,
            onBlur: this.props.onBlur,
            onFocus: this.props.onFocus,
            type: this.props.type,
            ref: (ref) => {
                this.textareaRef = ref;
                this.props.fieldRef(ref);

                if (ref && this.props.focus) {
                    ref.focus();
                }
            },
            placeholder: i18n.utils.translate({
                spec: this.props.spec,
                text: this.props.placeholder
            })
        };

        return (
            <textarea
                className={classNames([
                    theme.field,
                    this.props.validating
                        ? theme.validating
                        : "",
                    !this.props.isValid
                        ? theme.invalid
                        : ""
                ])}
                style={{
                    fontSize: `${this.props.scale}em`,
                    resize: this.props.resize
                }}
                {...props}
            />
        );
    }
}

export default FieldTextArea;