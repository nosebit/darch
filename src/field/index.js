import Error from "./error";
import Section from "./section";
import Text from "./text";
import TextArea from "./textarea";
import Password from "./password";

const Field = {
    Error,
    Section,
    Text,
    TextArea,
    Password
};

// Export
export default Field;