import { themr } from "react-css-themr";
import Component from "./component";
import styles from "./styles";
import wrap from "darch/src/utils/wrap";

let Composed;

// Compose with themr
Composed = themr(
    "FieldError",
    styles
)(Component);

// Final wrap.
Composed = wrap(Composed, "FieldErrorCmp");

// export
export default Composed;