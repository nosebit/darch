import React from "react";
import PropTypes from "prop-types";
import LoggerFactory from "darch/src/utils/logger";
import i18n from "darch/src/i18n";

const Logger = new LoggerFactory("field.error");

/**
 * This class defines the component.
 */
class FieldError extends React.Component {
    static propTypes = {
        for: PropTypes.string.isRequired,
        validator: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired
    };

    static defaultProps = {};

    /**
     * This function creates a new instance of this component.
     * 
     * @param {object} props - The component props.
     */
    constructor(props) {
        super(props);

        this.state = {
            active: false
        };
    }

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.debug("enter");

        if (this.props.form) {
            this.props.form.addFieldError(this);
        }
    }

    /**
     * This function is called when this component props/state gets updated.
     * 
     * @returns {void}
     */
    componentDidUpdate() {
        const logger = Logger.create("componentDidUpdate");
        logger.debug("enter");
    }

    /**
     * This function is called when component is about the get unmounted from the DOM.
     * 
     * @returns {void}
     */
    componentWillUnmount() {
        const logger = Logger.create("componentWillUnmount");
        logger.debug("enter");

        if (this.props.form) {
            this.props.form.removeFieldError(this);
        }
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        const { theme } = this.props;

        return this.state.active
            ? (
                <div className={theme.error}>
                    <i18n.Text
                        value={this.props.message}
                    />
                </div>
            )
            : null;
    }
}

export default FieldError;