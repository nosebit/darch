import lodash from "lodash";
import hogan from "hogan.js";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("i18n.utils");

/**
 * This class defines basic utilitaries methods for i18n module.
 */
class i18nUtils {
    static spec = null;

    /**
     * This function retrieves the translated path for a specific app route.
     * 
     * @param {object} routeId - The route id.
     * @param {object} opts - A map of options.
     * @param {object} opts.spec - The language spec.
     * @param {object} opts.params - A map of params to be injected into the route path.
     * @returns {string} The localized route path.
     */
    static routePath(
        routeId,
        {
            spec = i18nUtils.spec,
            params = null
        } = {}
    ) {
        const logger = Logger.create("routePath");
        logger.debug("enter", { routeId, params });

        let path = (lodash.get(spec, "routePaths") || {})[routeId];

        // Process path
        if (path && params) {
            for (const paramKey of Object.keys(params)) {
                path = path.replace(`:${paramKey}`, params[paramKey]);
            }
        }

        return path;
    }

    /**
     * This function performs a text translation.
     * 
     * @param {object} params - A map of params.
     * @param {object} params.spec - A map with language specs (dictionary of terms, etc).
     * @param {string} params.value - Text to be translated.
     * @param {object} params.data - A map of key/value pair to be replaced in text using hogan sintax.
     * @param {function} params.parse - A function to parse the resulted translation.
     * @param {string} params.shape - The shape of resulting translation (lower|upper|capital|camel).
     * @param {string} params.untranslatedDefault - Use this value in case we do not find a suitable translation for text.
     * @returns {string} The translated text.
     */
    static translate({
        spec = i18nUtils.spec,
        value = "",
        data = {},
        parse = (t) => t,
        shape = null,
        untranslatedDefault = null
    } = {}) {
        const logger = Logger.create("translate");
        logger.debug("enter", { value, data, spec });

        const dictionary = lodash.get(spec, "dictionary") || {};

        let translation = dictionary[value] || value;
        logger.debug("translation", { translation });

        if (untranslatedDefault && translation === value) {
            translation = dictionary[untranslatedDefault]
                || untranslatedDefault;
        }

        translation = hogan.compile(translation).render(data);
        logger.debug("translation : compiled", { translation });

        translation = parse(translation);
        logger.debug("translation : parsed", { translation });

        switch (shape) {
            case "lower":
                translation = lodash.lowerCase(translation);
                break;
            case "upper":
                translation = lodash.upperCase(translation);
                break;
            case "capital":
                translation = lodash.capitalize(translation);
                break;
            case "camel":
                translation = lodash.camelCase(translation);
                break;
            default:
        }

        logger.debug("leave", { translation });

        return translation;
    }
}

export default i18nUtils;