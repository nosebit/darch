import { connect } from "react-redux";
import Component from "./component";
import wrap from "darch/src/utils/wrap";

let Composed;

/**
 * Redux map state to props function.
 *
 * @param {object} state - The redux state
 * @param {object} ownProps - A list of props passed to the component.
 * @returns {object} The props the should be injected into the component.
 */
function mapStateToProps(state) {
    return {
        spec: state.i18n.spec
    };
}

/**
 * Redux dispatch to props map.
 */
const mapDispatchToProps = {

};

// Compose with redux
Composed = connect(
    mapStateToProps,
    mapDispatchToProps
)(Component);

// Final wrap.
Composed = wrap(Composed, "i18nTextCmp");

// export
export default Composed;