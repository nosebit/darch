import React from "react";
import PropTypes from "prop-types";
import lodash from "lodash";
import Utils from "../utils";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("i18n.text");

/**
 * This class defines the base component.
 */
class i18nText extends React.Component {
    static propTypes = {
        opts: PropTypes.object,
        value: PropTypes.string.isRequired,
        data: PropTypes.object,
        parse: PropTypes.func,
        format: PropTypes.string
    };

    static defaultProps = {
        opts: {},
        parse: (translation) => translation
    };

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.info("enter", { props: this.props });
    }

    /**
     * This function performs the translation of text value.
     * 
     * @returns {string} The value translated.
     */
    translate() {
        const logger = Logger.create("translate");
        logger.info("enter", { props: this.props });

        const spec = lodash.get(this.props, "spec");
        const {
            value,
            data,
            parse,
            format,
            untranslatedDefault
        } = this.props;

        return Utils.translate({
            spec, value, data, parse, 
            format, untranslatedDefault
        });
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        return (
            <span className={this.props.className}
                style={this.props.style}
                dangerouslySetInnerHTML={{
                    __html: this.translate()
                }}>
            </span>
        );
    }
}

export default i18nText;