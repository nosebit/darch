import { handleActions } from "redux-actions";
import Utils from "./utils";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("i18n.reducer");

// Reducer initial state.
const initialState = {
    spec: {
        formats: {},
        routePaths: {},
        dictionary: {}
    }
};

/**
 * This function handles i18nInit action completion.
 * 
 * @param {object} state - The current state of this reducer.
 * @param {object} action - The resolved action data.
 * @returns {object} The new state of this reducer.
 */
function i18NInit_COMPLETED(state, action) {
    const logger = Logger.create("i18nInit_COMPLETED");
    logger.debug("enter", { state, action });

    Utils.spec = action.payload;

    return { spec: action.payload };
}

// Export the reducer.
export default handleActions({
    i18NInit_COMPLETED
}, initialState);
