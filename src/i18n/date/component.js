import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import lodash from "lodash";
import LoggerFactory from "darch/src/utils/logger";

const Logger = new LoggerFactory("i18n.date");

/**
 * This class defines the base component.
 */
class i18nDate extends React.Component {
    static propTypes = {
        opts: PropTypes.object,
        value: PropTypes.string.isRequired,
        format: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.func
        ])
    };

    static defaultProps = {
        formatter: "datetime"
    };

    /**
     * This function is called when this component gets mounted into
     * the DOM.
     * 
     * @returns {void}
     */
    componentDidMount() {
        const logger = Logger.create("componentDidMount");
        logger.info("enter", { props: this.props });
    }

    /**
     * This function formats the date value.
     * 
     * @returns {string} Formated date value.
     */
    format() {
        const logger = Logger.create("format");
        logger.info("enter", { props: this.props });

        const spec = lodash.get(this.props, "spec");
        const { value, format } = this.props;
        const momentDate = moment(value);

        let date = value;

        if (momentDate.isValid()) {
            if (lodash.isString(format)) {
                const parsedFormat = lodash.get(spec.formats, format);
                date = momentDate.format(parsedFormat || format);

            } else if (lodash.isFunction(format)) {
                date = format(value);
            }
        }

        logger.debug("formatted date", { date });

        return date;
    }

    /**
     * This function renders this component into the DOM.
     * 
     * @returns {element} The component main element.
     */
    render() {
        return (
            <span className={this.props.className}
                style={this.props.style}>
                {this.format()}
            </span>
        );
    }
}

export default i18nDate;