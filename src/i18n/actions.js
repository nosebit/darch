import { createActions } from "redux-actions";
import LoggerFactory from "darch/src/utils/logger";
import Http from "darch/src/utils/http";

const Logger = new LoggerFactory("i18n.actions");
const http = new Http();

/**
 * This action initializes the i18n module fetching the language spec
 * file from the server.
 * 
 * @param {string} lang - The target language.
 * @param {object} opts - A map of options.
 * @param {string} opts.fallbackLang - A language to fallback if the spec file for target language is not found.
 * @param {path} opts.url - The base url to get language spec files.
 * @return {Promise} A promise that resolves to the spec file fetched.
 */
function i18NInit(
    lang = "en-us",
    {
        fallbackLang = "en-us",
        url = "/assets/i18n"
    } = {}
) {
    const logger = Logger.create("init");
    logger.debug("enter", { lang, fallbackLang });

    return http.request("GET", `${url}/${lang}.json`)
        .catch(() => (
            // Something went wrong : fetch fallback lang file.
            http.request("GET", `${url}/${fallbackLang}.json`)
        ));
}

// Export actions
export default createActions({
    i18NInit
});
