import Text from "./text";
import Date from "./date";
import actions from "./actions";
import reducer from "./reducer";
import utils from "./utils";

/**
 * The main class of this module
 */
const i18n = {
    Text,
    Date,
    actions,
    reducer,
    utils
};

// Export
export default i18n;