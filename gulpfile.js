/* eslint-disable flowtype/require-parameter-type */

const gulp = require("gulp");
const gutil = require("gulp-util");
const pug = require("gulp-pug");
const yaml = require("gulp-yaml");
const htmlmin = require("gulp-htmlmin");
const jsonmin = require("gulp-jsonmin");
const del = require("del");
const webpack = require("webpack");
const config = require("common-config");
const filepaths = require("./filepaths");

const statsInfo = {
    colors: gutil.colors.supportsColor,
    hash: false,
    timings: false,
    chunks: false,
    chunkModules: false,
    modules: false,
    children: true,
    version: true,
    cached: false,
    cachedAssets: false,
    reasons: false,
    source: false,
    errorDetails: false
};

const bundleDoneCalled = {};

/****************************************************************
* Clean Tasks : remove destination folders
****************************************************************/
gulp.task("clean:examples", function () {
    return del.sync([
        filepaths.examples.dest + "/assets",
        filepaths.examples.dest + "/index.html",
    ]);
});

gulp.task("clean:examples:dev", function () {
    return del.sync([
        filepaths.examples.devDest
    ]);
});

gulp.task("clean:lib", function () {
    return del.sync([
        filepaths.lib.dest + "/*"
    ]);
});

/****************************************************************
* Assets Task : copy assets to dist
****************************************************************/
gulp.task("assets:examples", function () {
    return gulp.src(filepaths.examples.src.assets)
        .pipe(gulp.dest(filepaths.examples.dest + "/assets"));
});

gulp.task("assets:examples:dev", function () {
    return gulp.src(filepaths.examples.src.assets)
        .pipe(gulp.dest(filepaths.examples.devDest + "/assets"));
});

/****************************************************************
* i18n Task : compile i18n yaml setups
****************************************************************/
gulp.task("i18n:examples", function () {
    return gulp.src(filepaths.examples.src.i18n)
        .pipe(yaml({ space: 4 }))
        .pipe(jsonmin())
        .pipe(gulp.dest(filepaths.examples.dest + "/assets/i18n"));
});

gulp.task("i18n:examples:dev", function () {
    return gulp.src(filepaths.examples.src.i18n)
        .pipe(yaml({ space: 4 }))
        .pipe(gulp.dest(filepaths.examples.devDest + "/assets/i18n"));
});

/****************************************************************
* Pug Task : compile pug templates
****************************************************************/
gulp.task("pug:examples", function () {
    return gulp.src(filepaths.examples.src.html)
        .pipe(pug({
            locals: config.webinfo
        }))
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(filepaths.examples.dest));
});

gulp.task("pug:examples:dev", function () {
    return gulp.src(filepaths.examples.src.html)
        .pipe(pug({
            locals: config.webinfo
        }))
        .pipe(gulp.dest(filepaths.examples.devDest));
});

/****************************************************************
* Bundle Examples Tasks : bundle all examples js files into one
****************************************************************/
gulp.task("bundle:examples:dev", function (done) {
    webpack(require("./webpack/examples.dev.config"), (err, stats) => {
        if (err) {
            throw new gutil.PluginError("webpack", err);
        }

        gutil.log("[webpack]", stats.toString(statsInfo));

        if (!bundleDoneCalled.examples) {
            bundleDoneCalled.examples = true;
            done();
        }
    });
});

/****************************************************************
* DEVELOPMENT TASK
****************************************************************/
gulp.task("development", [
    "clean:examples:dev",
    "assets:examples:dev",
    "pug:examples:dev",
    "i18n:examples:dev",
    "bundle:examples:dev",
], function () {
    gulp.watch(filepaths.examples.src.html, [ "pug:examples:dev" ]);
    gulp.watch(filepaths.examples.src.assets, [ "assets:examples" ]);
    gulp.watch(filepaths.examples.src.i18n, [ "i18n:examples:dev" ]);
    gulp.watch(filepaths.lib.src.js, [ "bundle:examples:dev" ]);
});

/****************************************************************
* DEFAULT TASK : Choose task by NODE_ENV
****************************************************************/
gulp.task("default", [
    process.env.NODE_ENV || "production"
]);