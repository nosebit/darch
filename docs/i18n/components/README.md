# i18n Components

The i18n module expose two components:

* [Text](/docs/i18n/components/text)
* [Date](/docs/i18n/components/date)