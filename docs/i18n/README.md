# i18n

This module is responsible for internationalization. All localized info should be placed in json files with name `LANG.json` where `LANG` stands for the lowercase version of [IETF language tag](https://en.wikipedia.org/wiki/IETF_language_tag) (like en-us or pt-br). The structure of this translation files should follows the following schema:

```json
{
    "lang": "pt-br",

    "formats": {
        "datetime": "DD/MM/YYYY HH:mm:ss",
        "date": "DD/MM/YYYY",
        "time": "HH:mm:ss",
        "number": "9.999,99"
        "currency": "R$ 9.999,99"
    },

    "routePaths": {
        "app": "/",
        "app.orders": "/pedidos",
        "app.orders.item": "/pedidos/:id"
    },

    "dictionary": {
        "Some text": "Algum texto"
    }
}
```

In this file we have:

* `lang` : the target language of the file (should be the same as in file name);
* `formats` : localization of any type of formats used for date and number;
* `routePath` : translations for all webapp routes where the key is the route id and the value is the localized route path;
* `dictionary` : a map of translations.


## Initialization

Before you can actually use this module you need to initialize it fetching the translation file for some target language. To do so, you should call the action `i18NInit` very early in your app lifecycle (in you root component maybe) like the following:

```jsx
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import i18n from "darch/lib/i18n";

class Root extend React.Component {
    componentDidMount() {
        await Promise.all([
            this.props.actions.i18NInit("pt-br", {
                fallbackLang = "en-us",
                url = "/assets/i18n"
            })
        ])

        this.setState({initialized: true});
    }

    render() {
        if(!this.state.initialized) {
            returns null;
        }

        return (
            <div>
                {'My App'}
            </div>
        )
    }
}

function mapStateToProps() {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({
            i18NInit: i18n.actions.i18NInit
        }, dispatch)
    };
}

/** Export **/
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Root);
```