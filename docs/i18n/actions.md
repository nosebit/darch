# i18n Actions

## `i18NInit(lang, opts)`

This function initializes the i18n module fetching and storing the translation file for the target language. This actions should be called as soon as possible in you app lifecycle.

##### Params
* `lang` : The target language which translation file should be loaded.
* `opts.fallbackLang` : A fallback language in case the translation file for target language is not found.
* `opts.url` : The url where we going to find the translation files (this url should reference the folder containing the files).

##### Return
A promise which is resolved when the file was successfully fetched and stored locally.

----------
