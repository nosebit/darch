# Darch [![npm][npm-badge]][npm-link] [![travis][travis-badge]][travis-link] [![coveralls][coveralls-badge]][coveralls-link] ![license][license-badge]

This framework implements common components and utilitaries for React used throughout Nosebit projects. The name sounds like "dark" and comes from recursive acronym DARCH Advanced React Components for Humans. Checkout the full documentation at http://darch.nosebit.com.

## Usage

Install Darch via npm running:

```bash
npm install --save darch
```

Then you can require each individual modules by:

```js
import Form from "darch/lib/form";

// For old school guys, you can do:
// let Form = require("darch/lib/form");
```

This individual module import is better to keep your code at bare minimum, but if you want to use all Darch then you can just:

```js
import {Form,Field,Container} from "darch";

// For old school guys, you have to do:
// let {Form,Field,Container} = require("darch");

// And for ES5 prehistoric guys:
// var Darch = require("darch");
// var Form = Darch.Form;
```

## Contributing

Help us to made Darch a great framework! See the the [contributing guidelines][contributing] for more information.

[contributing]: CONTRIBUTING.md
[documentation]: http://darch.nosebit.com
[npm-badge]: https://img.shields.io/npm/v/darch.svg?style=flat-square
[npm-link]: https://www.npmjs.com/package/darch
[travis-badge]: https://img.shields.io/travis/nosebit/darch/develop-0.1.0.svg?style=flat-square
[travis-link]: https://travis-ci.org/nosebit/darch.svg?branch=develop-1.0.0
[license-badge]: https://img.shields.io/npm/l/darch.svg?style=flat-square
[coveralls-badge]: https://img.shields.io/coveralls/nosebit/darch/develop-0.1.0.svg?style=flat-square
[coveralls-link]: https://coveralls.io/github/nosebit/darch?branch=develop-0.1.0