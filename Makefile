SHELL := /bin/bash
PATH := node_modules/.bin:$(PATH)

.PHONY: dev lint test_unit jsdoc docs docs_server server

dev:
	NODE_ENV=development gulp

lint:
	eslint ./*.js ./src/**/*.js

test_unit:
	NODE_ENV=test jest

jsdoc:
	jsdoc --configure .jsdoc.json --verbose --private

docs:
	cp README.md docs/
	gitbook build

docs_server:
	gitbook serve

server:
	http-server-spa ./examples/preview index.html 9000

